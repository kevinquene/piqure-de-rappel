-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : mar. 14 nov. 2023 à 15:17
-- Version du serveur : 10.4.28-MariaDB
-- Version de PHP : 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `piqure_rappel`
--

-- --------------------------------------------------------

--
-- Structure de la table `piqure_rappel_contact`
--

CREATE TABLE `piqure_rappel_contact` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `content` text NOT NULL,
  `status` varchar(10) NOT NULL,
  `object` varchar(50) NOT NULL,
  `answer_admin` text DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `view_at` datetime DEFAULT NULL,
  `answer_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `piqure_rappel_contact`
--

INSERT INTO `piqure_rappel_contact` (`id`, `id_user`, `content`, `status`, `object`, `answer_admin`, `created_at`, `view_at`, `answer_at`) VALUES
(1, 4, 'Je test cette fonctionnalitée.', 'resolved', 'Gros Test', 'Ok c\'est cool\r\n\r\nVraiment', '2023-11-13 22:07:13', '2023-11-14 00:06:40', '2023-11-14 10:18:55'),
(2, 5, 'Moi aussi je test', 'resolved', 'Petit Test', 'Coucou petit test', '2023-11-13 22:07:13', '2023-11-14 00:04:57', '2023-11-14 11:10:48'),
(3, 5, 'Moi je suis résolu', 'resolved', 'Résolu non ?', 'C\'est bon je l\'ai résolu', '2023-11-13 22:07:13', '2023-11-14 00:04:57', '2023-11-15 00:11:23'),
(4, 3, 'Ca bug vraiment beeeeeeeeaucoup', 'resolved', 'Bug site web', 'Olazla c\'est grave', '2023-11-14 14:47:39', '2023-11-14 14:47:45', '2023-11-14 14:51:20'),
(5, 3, 'A l\'aide mon ami', 'resolved', 'Problèmes de compte', 'C\'est bon tu epux pull', '2023-11-14 14:58:35', '2023-11-14 14:58:41', '2023-11-14 14:58:50');

-- --------------------------------------------------------

--
-- Structure de la table `piqure_rappel_user`
--

CREATE TABLE `piqure_rappel_user` (
  `id` int(11) NOT NULL,
  `name` varchar(120) DEFAULT NULL,
  `surname` varchar(120) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `sex` varchar(10) DEFAULT NULL,
  `email` varchar(120) NOT NULL,
  `phone_number` int(10) DEFAULT NULL,
  `social_security` varchar(13) NOT NULL,
  `password` varchar(255) NOT NULL,
  `tokens` varchar(255) NOT NULL,
  `role` varchar(11) NOT NULL,
  `status` varchar(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime DEFAULT NULL,
  `address` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `piqure_rappel_user`
--

INSERT INTO `piqure_rappel_user` (`id`, `name`, `surname`, `birthday`, `sex`, `email`, `phone_number`, `social_security`, `password`, `tokens`, `role`, `status`, `created_at`, `modified_at`, `address`) VALUES
(3, 'Leroux', 'Theo', '2004-03-07', 'Homme', 'theoleroux2004@gmail.com', 2147483647, '1234567891234', '$2y$10$XlxORogcA0UCcPiD2uYNIeWMwbEDQnVhVIs8KUb/CiyqPyzDx2SmC', 'NjsDth5e4lmw6vlieyPbXi06UnV3C0940XzaI8fUiftRLp1eYBnsJL9TPG227XWYolTLAulNbmYRr4uvcqtvrMMntqUe5aSMjOcYuSuqV5BhF36aHjEazUwubg5OzA4qLG', 'inscrit', 'admin', '2023-11-13 09:35:07', '2023-11-13 10:31:47', '30 rue Maugendre,76160,76160 DARNETAL'),
(4, 'Blackjack', 'Yuri', '2022-01-12', 'Homme', 'theolerouxpro0007@gmail.com', 652417668, '1237894561233', '$2y$10$9wCg3kIK33eS.V4emYP01.chGNWONRsQGhdIeFO4Drd0iXCETqex2', 'Ws7RaU2BY8MvT897WXfw01gYSGyxh6cDvfVkOuntOwUcdc00NpE4SDIs7C666ZL0h3qjq9QEM7jnMahaITVNR9BZ4RkV6Q5XT3VpWXc98hKuvMLWUlBhFVaADhxc85aUnS', 'inscrit', 'no', '2023-11-13 10:33:26', '2023-11-13 10:34:47', '30 rue Maugendre,76160,76160 DARNETAL'),
(5, 'Daphin', 'Dauplin', '1990-05-12', 'Autres', 'gedged@gmail.com', 652417668, '1231231231231', '$2y$10$9t9SobdXsTJMv.2Q4rzw4elws3Z8Mey4IseiR1wXL4YhzvK0lFwCu', '07dmdqCplnU07G7e2z9UOw6u6MmlK6H3SaoZeOPGerzOwhg4ENelfHKtcca8qXeSoeiLxtfzaZAzDhm266ROMoMyjCVtXpCEGpJENP6GfDdsE4Da14Qgku2bmAyNFFHv7C', 'inscrit', 'no', '2023-11-13 10:35:33', '2023-11-13 10:36:15', '30 Rue Maugendre,76160,76160 DARNETAL'),
(6, 'Petit', 'Poney', '1983-02-02', 'Homme', 'petitponey0007@gmail.com', 652417668, '9876543219876', '$2y$10$pMb4qdFF2d0FeruchDswzuEv9jiPX8UlEA.KS6uKR/AV6g5c3oVaS', 'QIr1PTwWbfxCdFAiXi8UKBCSvMo0fUff5aNWbP96hz6ecuSHwtHbcUSAzm2qMCk7C5ZAnkrccw5TEab7yXdoDUr7GUQzGpFV5E66R2IK0jBtAvvKNyPZ1nKy3zVux0MxFY', 'inscrit', 'no', '2023-11-13 10:37:28', '2023-11-13 10:38:16', '30 Rue Maugendre,76160,76160 DARNETAL'),
(7, 'Leroux', 'Théo', '1973-02-13', 'Homme', 'gdqgvqe@gmail.com', 682957141, '4561231237899', '$2y$10$BFo8IItyw3zpO2ajYN6T2OAGiUAai.5qshgS1Q00oQOQneDTkpbE2', 'd2pfCZppM1JuVquBZ2Xbiz2GYGuLzIq23mbBCOg4jItuE5JHo4qwDsOmeKT9HQJGaBg88ziqIiQnkctEkzNa4cdJeFCBExUnyEBVuLvjktxGnTSCWSJlMXMfMPJ7K8J935', 'inscrit', 'no', '2023-11-13 10:39:40', '2023-11-13 10:40:12', '30 Rue Maugendre,76160,76160 DARNETAL'),
(9, 'Edouart', 'Josette', '1900-01-11', 'Femme', 'fQKFHQOIFNIQO@gmail.com', 682957148, '1234567894566', '$2y$10$S5H27zMxebWaBbK1k4bvaOkqHfdvcCuxZjIOzfSCai2E6nd9YHEQu', 'HsgAbqpmtHcfWnphfETz1iiOmyvQHzN5ngg0EmV6GMSQMOP4FxTrPrVkWAF9iH8F889K8s3vhxRjatVYNdsGWQiqOUcAPlPHst8N7vfpiCmnQrHMTxuoW62aIRm0n1uJ76', 'inscrit', 'no', '2023-11-13 10:44:41', '2023-11-13 10:45:25', '30 Rue Maugendre,76160,76160 DARNETAL'),
(10, 'Leroux', 'Theo', '1970-02-04', 'Homme', 'skchquhfdoqd@gmail.com', 652417668, '1231234564523', '$2y$10$lVTq/RB8zC4psPmWZkkuRepgYwhCj67Iw/GXvw3Ewro9f/9cUdURS', 'IzgHK4LlSdwcdCFTtDUqQ8pQUZlzr46jvcV5ijLJe2YYSjBpz4SFS8v0oxajxjQ2aiKOdocWZN7bdbs5DM1Hds6XgSTRZAXrNBf9N12cHWmqCd4jJjVq3z3SGPn0JWJtTw', 'inscrit', 'no', '2023-11-13 10:46:15', '2023-11-13 10:46:42', 'Cergy,95000,CERGY'),
(11, NULL, NULL, NULL, NULL, 'theolerokjbux2004@gmail.com', NULL, '1231239874563', '$2y$10$Z.sfwj9El2hrJeVyHg5dTuy.l9uJKVKcGFgb/m1PYYuUmh0.TxUq.', '4TXCKFunObQzTIzZKQTflg1msv40jiniPxcS7t95QAQ0qmbNFhjFhhtWc9XX6teLP27R8v3kTOfubE0TIsTm44u09HddImuYhaM4r1s4ymQU3vW6cuLcIUyTiks9JscgZx', 'new', 'no', '2023-11-13 11:28:23', NULL, NULL),
(12, NULL, NULL, NULL, NULL, 'senjo@gmail.com', NULL, '4563214582586', '$2y$10$iomX4cJPBRRLnI6AMDBISuJ4CV/BJRwiQ6Po35FGTIvUfv5xXo0LG', 'C1zicjqROA3ILAWKG510EMGrFoHagoCkfx3Tdqsl9DE2BqeB4SHH8CEkAHfffV2sIl57BdnzDw2R5U81peX9UMnV0v271jbxBJWFWV9fj6aYmmWKDE9fzcIiPyDaAYGvOP', 'new', 'no', '2023-11-13 11:29:36', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `piqure_rappel_user_vaccin`
--

CREATE TABLE `piqure_rappel_user_vaccin` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_vaccin` int(11) NOT NULL,
  `secondary_effect` tinyint(1) DEFAULT NULL,
  `description_effect` text DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `vaccin_at` datetime NOT NULL,
  `num_lot` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `piqure_rappel_user_vaccin`
--

INSERT INTO `piqure_rappel_user_vaccin` (`id`, `id_user`, `id_vaccin`, `secondary_effect`, `description_effect`, `created_at`, `vaccin_at`, `num_lot`) VALUES
(4, 6, 29, NULL, NULL, '2023-11-13 10:52:45', '2023-11-13 10:52:45', ''),
(6, 4, 91, 1, NULL, '2023-11-13 10:52:45', '2023-11-13 10:52:45', ''),
(7, 5, 64, 1, NULL, '2023-11-13 10:52:45', '2023-11-13 10:52:45', ''),
(15, 7, 25, 1, NULL, '2023-11-13 10:52:45', '2023-11-13 10:52:45', ''),
(16, 3, 25, NULL, NULL, '2023-11-13 10:52:45', '2023-11-13 10:52:45', ''),
(17, 6, 25, NULL, NULL, '2023-11-13 10:52:45', '2023-11-13 10:52:45', ''),
(18, 10, 25, NULL, NULL, '2023-11-13 10:52:45', '2023-11-13 10:52:45', ''),
(19, 9, 25, NULL, NULL, '2023-11-13 10:52:45', '2023-11-13 10:52:45', ''),
(20, 3, 59, NULL, NULL, '2023-11-13 10:52:45', '2023-11-13 10:52:45', ''),
(21, 4, 88, 1, NULL, '2023-11-13 10:52:45', '2023-11-13 10:52:45', ''),
(22, 5, 11, 1, NULL, '2023-11-13 10:52:45', '2023-11-13 10:52:45', ''),
(23, 6, 4, NULL, NULL, '2023-11-13 10:52:45', '2023-11-13 10:52:45', ''),
(24, 6, 9, NULL, NULL, '2023-11-13 10:52:45', '2023-11-13 10:52:45', ''),
(26, 7, 8, 1, NULL, '2023-11-13 10:52:45', '2023-11-13 10:52:45', ''),
(30, 9, 9, NULL, NULL, '2023-11-13 10:52:45', '2023-11-13 10:52:45', ''),
(31, 10, 66, NULL, NULL, '2023-11-13 10:52:45', '2023-11-13 10:52:45', ''),
(32, 4, 91, 1, NULL, '2023-11-13 10:52:45', '2023-11-13 10:52:45', ''),
(33, 4, 88, 1, NULL, '2023-11-13 10:52:45', '2023-11-13 10:52:45', ''),
(34, 5, 11, 1, NULL, '2023-11-13 10:52:45', '2023-11-13 10:52:45', ''),
(35, 6, 29, NULL, NULL, '2023-11-13 10:52:45', '2023-11-13 10:52:45', ''),
(36, 6, 25, NULL, NULL, '2023-11-13 10:52:45', '2023-11-13 10:52:45', '');

-- --------------------------------------------------------

--
-- Structure de la table `piqure_rappel_vaccin`
--

CREATE TABLE `piqure_rappel_vaccin` (
  `id` int(11) NOT NULL,
  `name` varchar(120) NOT NULL,
  `content` text NOT NULL,
  `status` varchar(30) NOT NULL,
  `rappel_day` varchar(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime DEFAULT NULL,
  `created_by` varchar(120) NOT NULL,
  `modified_by` varchar(120) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `piqure_rappel_vaccin`
--

INSERT INTO `piqure_rappel_vaccin` (`id`, `name`, `content`, `status`, `rappel_day`, `created_at`, `modified_at`, `created_by`, `modified_by`) VALUES
(1, 'Tétravac-Acellulaire®', 'Diphtérie', '', '3 72', '2023-11-12 12:49:27', NULL, '', NULL),
(2, 'Tétravac-Acellulaire®', 'Tétanos', '', '3 72', '2023-11-12 12:50:07', NULL, '', NULL),
(3, 'Tétravac-Acellulaire®', 'Poliomyélite', '', '3 72', '2023-11-12 12:50:39', NULL, '', NULL),
(5, 'Tétravac-Acellulaire®', 'Coqueluche', '', '3 72', '2023-11-12 12:51:19', NULL, '', NULL),
(6, 'Revaxis®', 'Diphtérie', '', 'infini 120', '2023-11-12 12:52:15', NULL, '', NULL),
(7, 'Revaxis®', 'Tétanos', '', 'infini 120', '2023-11-12 12:52:33', NULL, '', NULL),
(8, 'Revaxis®', 'Poliomyélite', '', 'infini 120', '2023-11-12 12:52:51', NULL, '', NULL),
(9, 'Infanrix Tetra®', 'Diphtérie', '', '1 6', '2023-11-12 12:53:32', NULL, '', NULL),
(10, 'Infanrix Tetra®', 'Tétanos', '', '1 6', '2023-11-12 12:53:52', NULL, '', NULL),
(11, 'Infanrix Tetra®', 'Poliomyélite', '', '1 6', '2023-11-12 12:54:02', NULL, '', NULL),
(12, 'Infanrix Tetra®', 'Coqueluche', '', '1 6', '2023-11-12 12:54:17', NULL, '', NULL),
(13, 'Repevax®', 'Diphtérie', '', 'infini 60', '2023-11-12 12:54:55', NULL, '', NULL),
(14, 'Repevax®', 'Tétanos', '', 'infini 60', '2023-11-12 12:55:03', NULL, '', NULL),
(15, 'Repevax®', 'Poliomyélite', '', 'infini 60', '2023-11-12 12:55:15', NULL, '', NULL),
(16, 'Repevax®', 'Coqueluche', '', 'infini 60', '2023-11-12 12:55:24', NULL, '', NULL),
(17, 'Boostrixtera®', 'Diphtérie', '', '2 3', '2023-11-12 12:56:31', NULL, '', NULL),
(18, 'Boostrixtera®', 'Tétanos', '', '2 3', '2023-11-12 12:56:39', NULL, '', NULL),
(19, 'Boostrixtera®', 'Poliomyélite', '', '2 3', '2023-11-12 12:56:52', NULL, '', NULL),
(20, 'Boostrixtera®', 'Coqueluche', '', '2 3', '2023-11-12 12:57:00', NULL, '', NULL),
(21, 'Infanrix Quinta®', 'Diphtérie', '', '2 1', '2023-11-12 12:58:11', NULL, '', NULL),
(22, 'Infanrix Quinta®', 'Tétanos', '', '2 1', '2023-11-12 12:58:19', NULL, '', NULL),
(23, 'Infanrix Quinta®', 'Poliomyélite', '', '2 1', '2023-11-12 12:58:32', NULL, '', NULL),
(24, 'Infanrix Quinta®', 'Coqueluche', '', '2 1', '2023-11-12 12:58:44', NULL, '', NULL),
(25, 'Infanrix Quinta®', 'Méningites à Haemophilus influenzae B', '', '2 1', '2023-11-12 12:59:34', NULL, '', NULL),
(26, 'Pentavac®', 'Diphtérie', '', '1 8', '2023-11-12 13:00:07', NULL, '', NULL),
(27, 'Pentavac®', 'Tétanos', '', '2 5', '2023-11-12 13:00:16', NULL, '', NULL),
(28, 'Pentavac®', 'Poliomyélite', '', '2 5', '2023-11-12 13:00:25', NULL, '', NULL),
(29, 'Pentavac®', 'Coqueluche', '', '2 5', '2023-11-12 13:00:32', NULL, '', NULL),
(30, 'Pentavac®', 'Méningites à Haemophilus influenzae B', '', '2 5', '2023-11-12 13:01:04', NULL, '', NULL),
(31, 'Hexyon®', 'Diphtérie', '', '2 4', '2023-11-12 13:01:36', NULL, '', NULL),
(32, 'Hexyon®', 'Tétanos', '', '2 4', '2023-11-12 13:02:09', NULL, '', NULL),
(33, 'Hexyon®', 'Poliomyélite', '', '2 4', '2023-11-12 13:03:55', NULL, '', NULL),
(34, 'Hexyon®', 'Coqueluche', '', '2 4', '2023-11-12 13:04:04', NULL, '', NULL),
(35, 'Hexyon®', 'Méningites à Haemophilus influenzae B', '', '2 4', '2023-11-12 13:04:30', NULL, '', NULL),
(36, 'Hexyon®', 'Hépatite B', '', '2 4', '2023-11-12 13:04:49', NULL, '', NULL),
(37, 'Infanrix Hexa®', 'Diphtérie', '', '1 6', '2023-11-12 13:07:31', NULL, '', NULL),
(38, 'Infanrix Hexa®', 'Tétanos', '', '1 6', '2023-11-12 13:08:10', NULL, '', NULL),
(39, 'Infanrix Hexa®', 'Poliomyélite', '', '1 6', '2023-11-12 13:08:23', NULL, '', NULL),
(40, 'Infanrix Hexa®', 'Coqueluche', '', '1 6', '2023-11-12 13:08:37', NULL, '', NULL),
(41, 'Infanrix Hexa®', 'Méningites à Haemophilus influenzae B', '', '1 6', '2023-11-12 13:09:04', NULL, '', NULL),
(42, 'Infanrix Hexa®', 'Hépatite B', '', '1 6', '2023-11-12 13:09:51', NULL, '', NULL),
(43, 'Vaxelis®', 'Diphtérie', '', '1 72', '2023-11-12 13:10:35', NULL, '', NULL),
(44, 'Vaxelis®', 'Tétanos', '', '1 72', '2023-11-12 13:10:43', NULL, '', NULL),
(45, 'Vaxelis®', 'Poliomyélite', '', '1 72', '2023-11-12 13:10:55', NULL, '', NULL),
(46, 'Vaxelis®', 'Coqueluche', '', '1 72', '2023-11-12 13:11:09', NULL, '', NULL),
(47, 'Vaxelis®', 'Méningites à Haemophilus influenzae B', '', '1 72', '2023-11-12 13:11:33', NULL, '', NULL),
(48, 'Vaxelis®', 'Hépatite B', '', '1 72', '2023-11-12 13:11:45', NULL, '', NULL),
(49, 'Imovax Polio®', 'Poliomyélite', '', '1 12', '2023-11-12 13:14:20', NULL, '', NULL),
(50, 'Act-Hib®', 'Méningites à Haemophilus influenzae B', '', '1 2', '2023-11-12 13:16:05', NULL, '', NULL),
(51, 'Pneumovax®', 'Méningites, pneumonies et septicémies à pneumocoque', '', '1 8', '2023-11-12 13:19:10', NULL, '', NULL),
(52, 'Prevenar 13®', 'Méningites, pneumonies et septicémies à pneumocoque', '', '1 12', '2023-11-12 13:20:47', NULL, '', NULL),
(53, 'Engirix B 10®', 'Hépatite B', '', '1 12', '2023-11-12 13:23:20', NULL, '', NULL),
(54, 'HBVAXPRO 5®', 'Hépatite B', '', '1 12', '2023-11-12 13:23:56', NULL, '', NULL),
(55, 'Engirix B 20®', 'Hépatite B', '', '1 12', '2023-11-12 13:24:17', NULL, '', NULL),
(56, 'HBVAXPRO 10®', 'Hépatite B', '', '1 12', '2023-11-12 13:24:25', NULL, '', NULL),
(57, 'Twinrix Adulte®', 'Hépatite B', '', '2 4', '2023-11-12 13:26:53', NULL, '', NULL),
(58, 'Priorix M-M-RVaxpro®', 'Rougeole', '', '0 0 ', '2023-11-12 13:33:01', NULL, '', NULL),
(59, 'Priorix M-M-RVaxpro®', 'Oreillons', '', '0 0', '2023-11-12 13:33:11', NULL, '', NULL),
(60, 'Priorix M-M-RVaxpro®', 'Rubéole', '', '0 0', '2023-11-12 13:33:20', NULL, '', NULL),
(61, 'Trumemba®', 'Méningites et septicémies à méningocoque', '', '1 6', '2023-11-12 13:42:36', NULL, '', NULL),
(62, 'Menquadfi®', 'Méningites et septicémies à méningocoque', '', '1 7', '2023-11-12 13:43:10', NULL, '', NULL),
(63, 'Bexsero®', 'Méningites et septicémies à méningocoque', '', '1 12', '2023-11-12 13:43:19', NULL, '', NULL),
(64, 'Nimenrix®', 'Méningites et septicémies à méningocoque', '', '1 2', '2023-11-12 13:43:28', NULL, '', NULL),
(65, 'Menveo®', 'Méningites et septicémies à méningocoque', '', '1 60', '2023-11-12 13:43:36', NULL, '', NULL),
(66, 'Menjugate 10®', 'Méningites et septicémies à méningocoque', '', '1 12', '2023-11-12 13:43:47', NULL, '', NULL),
(67, 'Neisvac®', 'Méningites et septicémies à méningocoque', '', '1 6', '2023-11-12 13:44:01', NULL, '', NULL),
(68, 'BCG®', 'Tuberculose', '', '0 0', '2023-11-12 13:57:18', NULL, '', NULL),
(69, 'Vaqta 50®', 'Hépatite A', '', '1 6', '2023-11-12 14:07:36', NULL, '', NULL),
(70, 'Avaxim 80®', 'Hépatite A', '', '1 6', '2023-11-12 14:07:52', NULL, '', NULL),
(71, 'Havrix 720®', 'Hépatite A', '', '1 6', '2023-11-12 14:08:07', NULL, '', NULL),
(72, 'Avaxim 160®', 'Hépatite A', '', '1 6', '2023-11-12 14:08:23', NULL, '', NULL),
(73, 'Havrix 1440®', 'Hépatite A', '', '1 6', '2023-11-12 14:08:34', NULL, '', NULL),
(74, 'Tyavax®', 'Hépatite A', '', '1 6', '2023-11-12 14:08:48', NULL, '', NULL),
(75, 'Twinrix Adulte®', 'Hépatite A', '', '1 6', '2023-11-12 14:08:58', NULL, '', NULL),
(76, 'Gardasil 9®', 'Infections à Papillomavirus humains (HPV)', '', '2 3', '2023-11-12 14:15:29', NULL, '', NULL),
(77, 'Cervarix®', 'Infections à Papillomavirus humains (HPV)', '', '2 3', '2023-11-12 14:15:39', NULL, '', NULL),
(78, 'Efluelda®', 'Grippe', '', '0 0', '2023-11-12 14:19:05', NULL, '', NULL),
(79, 'Fluarix Tetra®', 'Grippe', '', '0  0', '2023-11-12 14:19:17', NULL, '', NULL),
(80, 'Influva Tetra®', 'Grippe', '', '0 0', '2023-11-12 14:19:32', NULL, '', NULL),
(81, 'Vaxigrip Tetra®', 'Grippe', '', '0 0', '2023-11-12 14:19:44', NULL, '', NULL),
(82, 'Comirnaty Omicron XBB 1.5®', 'Covid-19', '', '3 3', '2023-11-12 14:28:22', NULL, '', NULL),
(83, 'Comirnaty Omicron XBB 1.5 (pédiatrique) 3 microgrammes/dose®', 'Covid-19', '', '3 3', '2023-11-12 14:28:47', NULL, '', NULL),
(84, 'Comirnaty Omicron XBB 1.5 (pédiatrique) 10 microgrammes/dose®', 'Covid-19', '', '3 3', '2023-11-12 14:28:56', NULL, '', NULL),
(85, 'Nuvaxovid®', 'Covid-19', '', '1 6', '2023-11-12 14:29:14', NULL, '', NULL),
(86, 'VidPrevtyn Beta®', 'Covid-19', '', '1 6', '2023-11-12 14:29:31', NULL, '', NULL),
(87, 'Varilrix®', 'Varicelle', '', '1 2', '2023-11-12 14:34:26', NULL, '', NULL),
(88, 'Varivax®', 'Varicelle', '', '1 2', '2023-11-12 14:34:35', NULL, '', NULL),
(89, 'Stamaril®', 'Fièvre jaune', '', '0 0', '2023-11-12 14:38:19', NULL, '', NULL),
(90, 'ZOSTAVAX®', 'Zona', '', '0 0', '2023-11-12 14:41:31', NULL, '', NULL),
(91, 'Rotarix®', 'Rotavirus (gastro-entérite)', '', '1 1', '2023-11-12 14:45:51', NULL, '', NULL),
(92, 'RotaTeq®', 'Rotavirus (gastro-entérite)', '', '1 1', '2023-11-12 14:46:01', NULL, '', NULL),
(93, 'Tétravacacellulaire®', 'Diphtérie/Tétanos/Poliomyélite/Coqueluche', '', '1 2', '2023-11-12 14:56:03', NULL, '', NULL),
(94, 'Revaxis®', 'Diphtérie/Tétanos/Poliomyélite', '', '1 2', '2023-11-12 14:56:25', NULL, '', NULL),
(95, 'Infanrix Tetra®', 'Diphtérie/Tétanos/Poliomyélite/Coqueluche', '', '0 0', '2023-11-12 14:57:16', NULL, '', NULL),
(96, 'Repevax®', 'Diphtérie/Tétanos/Poliomyélite/Coqueluche', '', '0 0', '2023-11-12 14:57:33', NULL, '', NULL),
(97, 'Boostrixtera®', 'Diphtérie/Tétanos/Poliomyélite/Coqueluche', '', '2 3', '2023-11-12 14:57:46', NULL, '', NULL),
(98, 'Infanrix Quinta®', 'Diphtérie/Tétanos/Poliomyélite/Coqueluche/Méningites à Haemophilus influenzae B', '', '2 1', '2023-11-12 14:58:27', NULL, '', NULL),
(99, 'Pentavac®', 'Diphtérie/Tétanos/Poliomyélite/Coqueluche/Méningites à Haemophilus influenzae B', '', '1 9', '2023-11-12 14:58:35', NULL, '', NULL),
(100, 'Hexyon®', 'Diphtérie/Tétanos/Poliomyélite/Coqueluche/Méningites à Haemophilus influenzae B/Hépatite B', '', '1 6', '2023-11-12 14:59:03', NULL, '', NULL),
(101, 'Infanrix Hexa®', 'Diphtérie/Tétanos/Poliomyélite/Coqueluche/Méningites à Haemophilus influenzae B/Hépatite B', '', '1 6', '2023-11-12 14:59:32', NULL, '', NULL),
(102, 'Vaxelis®', 'Diphtérie/Tétanos/Poliomyélite/Coqueluche/Méningites à Haemophilus influenzae B/Hépatite B', '', '1 6', '2023-11-12 14:59:44', NULL, '', NULL),
(103, 'Twinrix Adulte®', 'Hépatite A/Hépatite B', '', '1 6', '2023-11-12 15:01:09', NULL, '', NULL),
(104, 'Priorix M-M-RVaxpro®', 'Rougeole/Oreillons/Rubéole', '', '0 0', '2023-11-12 15:01:40', NULL, '', NULL),
(105, 'Tyavax®', 'Hépatite A/Fièvre typhoïde', '', '1 36', '2023-11-12 15:02:31', NULL, '', NULL);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `piqure_rappel_contact`
--
ALTER TABLE `piqure_rappel_contact`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `piqure_rappel_user`
--
ALTER TABLE `piqure_rappel_user`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `piqure_rappel_user_vaccin`
--
ALTER TABLE `piqure_rappel_user_vaccin`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `piqure_rappel_vaccin`
--
ALTER TABLE `piqure_rappel_vaccin`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `piqure_rappel_contact`
--
ALTER TABLE `piqure_rappel_contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `piqure_rappel_user`
--
ALTER TABLE `piqure_rappel_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT pour la table `piqure_rappel_user_vaccin`
--
ALTER TABLE `piqure_rappel_user_vaccin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT pour la table `piqure_rappel_vaccin`
--
ALTER TABLE `piqure_rappel_vaccin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php
require('asset/inc/pdo2.php');
require('asset/inc/fonction.php');
require('asset/inc/validation.php');


if (isBanned()){
    $_SESSION=array();
    header("Location: https://www.youtube.com/watch?v=dQw4w9WgXcQ");
}

include('asset/inc/header.php');


?>
<section id="accueil">
    <div class="wrap2">
        <div class="box_connexion">
            <h1>Vaccinez votre avenir, <br>maintenant!</h1>
            <?php if (isLogged()){
                $user = $_SESSION['user']['id'];?>
                    <a href="moncarnet_index.php?id=<?php echo $user ; ?>">MON CARNET</a>
            <?php }else{ ?>
                <a href="inscription.php">INSCRIPTION</a>
            <?php } ?>
        </div>
        <div class="box_img">
            <img src="asset/img/harold_accueil.png" class="harold" alt="homme souriant">
            <img src="asset/img/image_background_accueil.svg"  class="design" alt="design">
            <img src="asset/img/image_background_accueil_full.png"  class="design_full" alt="design">
        </div>
    </div>
</section>
<section id="presentation">
    <div class="wrap1">
        <h2>Piqûre De Rappel...c'est quoi?</h2>
        <div class="description">
            <p>Parce qu'il est important de se protéger et de rester en bonne santé!  </br> De pouvoir profiter pleinement des moments de la vie et de ses proches! </p>
            <p>Et car cela nous tient à coeur, nous nous sommes donnés la mission de vous aider dans cette tâche! </p>
            <p>Pour cela nous avons créé <span class="piqure">Piqûre De Rappel</span>!</p>
            <p>Piqûre De Rappel est une application Web qui vous permet de suivre et de gérer votre vaccination sous la forme d'un carnet en ligne.</p>

        </div>
    </div>


    </div>
</section>


<?php
include ('asset/inc/footer.php');



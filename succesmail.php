<?php
require('asset/inc/pdo2.php');
require('asset/inc/fonction.php');
require('asset/inc/validation.php');

require 'vendor/phpmailer/phpmailer/src/PHPMailer.php';
require 'vendor/phpmailer/phpmailer/src/SMTP.php';
require 'vendor/phpmailer/phpmailer/src/Exception.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
if (isBanned()){
    $_SESSION=array();
    header("Location: https://www.youtube.com/watch?v=dQw4w9WgXcQ");
}
if (isLogged()){
    $user = $_SESSION['user']['id'];
    header("Location: moncarnet_index.php?id=$user");
}

$titre = 'Oublie de Mot de Passe - PIQÛRE DE RAPPEL';

if (isset($_SESSION['recupmdp'])){

    $_SESSION['recupmdp']['antispam'] = true;
    $lienmessage=$_SESSION['recupmdp']['chemin']. '?tokens=' .$_SESSION['recupmdp']['tokens'];
    $sendmail = new PHPMailer(true);

    // Paramètres du serveur SMTP
    $sendmail->isSMTP();
    $sendmail->Host = 'smtp-mail.outlook.com';  ///
    $sendmail->SMTPAuth = true;
    $sendmail->Username = 'piqurederappelpro@outlook.fr';   ////
    $sendmail->Password = 'Azerty12.';   ///
    $sendmail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
    $sendmail->Port = 587;

    // Encodage UTF-8
    $sendmail->CharSet = 'UTF-8';
    $sendmail->Encoding = 'base64';

    // Destinataire et contenu
    $sendmail->setFrom('piqurederappelpro@outlook.fr', 'Piqure de rappel');
    $sendmail->addAddress($_SESSION['recupmdp']['email'], $_SESSION['recupmdp']['email']);
    $sendmail->isHTML(true);
    $sendmail->Subject = 'Récupération de Mot de passe';
    $sendmail->Body = '<body style="font-family: \'Arial\', sans-serif; background-color: #f0f8ff; color: #333; text-align: center; margin: 0; padding: 0;">

    <div style="max-width: 600px; margin: 50px auto; background-color: #ffffff; padding: 20px; border-radius: 8px; box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);">
        <h2 style="color: #3498db;">Récupération de Mot de Passe</h2>
        <p style="margin-bottom: 20px;">Vous avez demandé une récupération de mot de passe. Cliquez sur le lien ci-dessous pour réinitialiser votre mot de passe :</p>
        <a style="display: inline-block; padding: 10px 20px; background-color: #3498db; color: #fff; text-decoration: none; border-radius: 5px; transition: background-color 0.3s;" href="'.$lienmessage.'">Réinitialiser le Mot de Passe</a>
        <p style="margin-top: 25px; font-style: italic; color: red; font-size: 10px">*Veuillez ignorer ce mail si vous n\'êtes pas à l\'origine de cette demande</p>
    </div>

</body>';

    // Envoyer l'email
    $sendmail->send();

include('asset/inc/header.php'); ?>

<section id="succesmail">
    <div class="wrap1">

        <div class="bloc-succesmail">
            <h1>Le mail de récupération vous a bien était envoyé</h1>
            <a href="">Me renvoyer mon mail</a>
            <a href="mdpoublie.php">Je me suis trompé d'adresse mail !!</a>
        </div>
    </div>
</section>

<?php
include ('asset/inc/footer.php');
}else{
    header('Location: 404.php');
}
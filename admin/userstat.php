<?php
require('../asset/inc/pdo2.php');
require('../asset/inc/fonction.php');
require('asset/inc/statistique.php');
require('asset/inc/validation.php');
if (isBanned()){
    $_SESSION=array();
    header("Location: https://www.youtube.com/watch?v=dQw4w9WgXcQ");
}
if (!isAdmin()){
    header("Location: ../403.php");
}
$titre = 'Admin Statistique des Utilisateurs - PIQÛRE DE RAPPEL';
include('asset/inc/header.php');
?>

<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4 grandtitrehauthome">
        <h1 class="h3 mb-0 text-gray-800">Tableau de bord</h1>
    </div>

    <div class="row">


        <div class="col-xl-4 col-lg-5">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Genres des Inscrits</h6>
                    <div class="dropdown no-arrow">
                    </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <div class="chart-pie pt-4 pb-2">
                        <canvas id="camembertCanvas"></canvas>
                    </div>
                    <div class="mt-4 text-center small">
                                        <span class="mr-2">
                                            <i class="fa-solid fa-circle" style="color: #006eff;"></i> Hommes
                                        </span>
                        <span class="mr-2">
                                            <i class="fa-solid fa-circle" style="color: #ff475a;"></i> Femmes
                                        </span>
                        <span class="mr-2">
                                            <i class="fa-solid fa-circle" style="color: #e0f000;"></i> Autres
                                        </span>
                    </div>
                </div>
            </div>
        </div>

        <!-- Area Chart -->
        <div class="col-xl-8 col-lg-7">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div
                        class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Nombre d'Utilisateurs</h6>
                    <div class="dropdown no-arrow">
                    </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <div class="chart-area">
                        <?php if (!empty($nombre18)){ ?>
                        <canvas id="barCanvas"></canvas>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>


        <!-- Pie Chart -->
    </div>
    <div class="row">

        <!-- Content Column -->
        <div class="col-lg-6 mb-4">

            <!-- Project Card Example -->
            <div class="card shadow mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Les plus vaccinés</h6>
                    <div class="dropdown no-arrow">
                    </div>
                </div>
                <div class="card-body"><?php
                    if(isset($user5['name'][0])){ ?>
                    <h4 class="small font-weight-bold"><?= $user5['name'][0] ?> <span
                                class="float-right"><?= $user5['nbr'][0] ?></span></h4>
                    <div class="progress mb-4">
                        <div class="progress-bar bg-danger" role="progressbar" style="width: <?= $user5['prc'][0] ?>%"
                             aria-valuenow="<?= $user5['nbr'][0] ?>" aria-valuemin="0" aria-valuemax="<?= $user5['nbr'][0] ?>>"></div>
                    </div>
                    <?php }if(isset($user5['name'][1])){ ?>
                    <h4 class="small font-weight-bold"><?= $user5['name'][1] ?> <span
                                class="float-right"><?=$user5['nbr'][1]?></span></h4>
                    <div class="progress mb-4">
                        <div class="progress-bar bg-orange" role="progressbar" style="width: <?= $user5['prc'][1] ?>%"
                             aria-valuenow="<?= $user5['nbr'][1] ?>" aria-valuemin="0" aria-valuemax="<?= $user5['nbr'][0] ?>"></div>
                    </div>
                    <?php }if(isset($user5['name'][2])){ ?>
                    <h4 class="small font-weight-bold"><?= $user5['name'][2] ?> <span
                                class="float-right"><?=$user5['nbr'][2]?></span></h4>
                    <div class="progress mb-4">
                        <div class="progress-bar bg-warning" role="progressbar" style="width: <?= $user5['prc'][2] ?>%"
                             aria-valuenow="<?= $user5['nbr'][2] ?>" aria-valuemin="0" aria-valuemax="<?= $user5['nbr'][0] ?>"></div>
                    </div>
                    <?php }if(isset($user5['name'][3])){ ?>
                    <h4 class="small font-weight-bold"><?= $user5['name'][3] ?> <span
                                class="float-right"><?=$user5['nbr'][3]?></span></h4>
                    <div class="progress mb-4">
                        <div class="progress-bar " role="progressbar" style="width: <?= $user5['prc'][3] ?>%"
                             aria-valuenow="<?= $user5['nbr'][3] ?>" aria-valuemin="0" aria-valuemax="<?= $user5['nbr'][0] ?>"></div>
                    </div>
                    <?php }if(isset($user5['name'][4])){ ?>
                    <h4 class="small font-weight-bold"><?= $user5['name'][4] ?> <span
                                class="float-right"><?= $user5['nbr'][4] ?></span></h4>
                    <div class="progress mb-4">
                        <div class="progress-bar bg-info" role="progressbar" style="width: <?= $user5['prc'][4] ?>%"
                             aria-valuenow="<?= $user5['nbr'][4] ?>" aria-valuemin="0" aria-valuemax="<?= $user5['nbr'][0] ?>"></div>
                    </div>
                    <?php } ?>
                </div>
            </div>

        </div>

        <div class="col-lg-6 mb-4">

            <div class="card shadow mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Nouveaux Utilisateurs</h6>
                    <div class="dropdown no-arrow">
                    </div>
                </div>
                <div class="card-body">
                    <table>
                        <tr>
                            <th>Nom</th>
                            <th class="prenom-table">Prenom</th>
                            <th>Genre</th>
                            <th>Age</th>
                        </tr>
                        <?php if (!empty($newusers[0])){ ?>
                        <tr>
                            <td><?= $newusers[0]['name'] ?></td>
                            <td class="prenom-table"><?= $newusers[0]['surname'] ?></td>
                            <td><?= $newusers[0]['sex'] ?></td>
                            <td><?= agecalcul(0) ?></td>
                        </tr>
                        <?php }if (!empty($newusers[1])){ ?>
                        <tr>
                            <td><?= $newusers[1]['name'] ?></td>
                            <td class="prenom-table"><?= $newusers[1]['surname'] ?></td>
                            <td><?= $newusers[1]['sex'] ?></td>
                            <td><?= agecalcul(1) ?></td>
                        </tr>
                        <?php }if (!empty($newusers[2])){ ?>
                        <tr>
                            <td><?= $newusers[2]['name'] ?></td>
                            <td class="prenom-table"><?= $newusers[2]['surname'] ?></td>
                            <td><?= $newusers[2]['sex'] ?></td>
                            <td><?= agecalcul(2) ?></td>
                        </tr>
                        <?php }if (!empty($newusers[3])){ ?>
                        <tr>
                            <td><?= $newusers[3]['name'] ?></td>
                            <td class="prenom-table"><?= $newusers[3]['surname'] ?></td>
                            <td><?= $newusers[3]['sex'] ?></td>
                            <td><?= agecalcul(3) ?></td>
                        </tr>
                        <?php }if (!empty($newusers[4])){ ?>
                        <tr>
                            <td><?= $newusers[4]['name'] ?></td>
                            <td class="prenom-table"><?= $newusers[4]['surname'] ?></td>
                            <td><?= $newusers[4]['sex'] ?></td>
                            <td><?= agecalcul(4) ?></td>
                        </tr>
                        <?php }if (!empty($newusers[5])){ ?>
                        <tr>
                            <td><?= $newusers[5]['name'] ?></td>
                            <td class="prenom-table"><?= $newusers[5]['surname'] ?></td>
                            <td><?= $newusers[5]['sex'] ?></td>
                            <td><?= agecalcul(5) ?></td>
                        </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>

        </div>
    </div>
</div>


</div>
</div>
<?php
require('asset/inc/diagramme.php');
include('asset/inc/footer.php');
?>



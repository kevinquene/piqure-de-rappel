<?php
require('../asset/inc/pdo2.php');
require('../asset/inc/fonction.php');
require('asset/inc/validation.php');
if (isBanned()){
    $_SESSION=array();
    header("Location: https://www.youtube.com/watch?v=dQw4w9WgXcQ");
}
if (!isAdmin()){
    header("Location: ../403.php");
}
$titre = 'Admin Gestion utilisateurs - PIQÛRE DE RAPPEL';
$getid =$_GET['id'];
global $pdo;
$sql = "SELECT *
        FROM piqure_rappel_vaccin AS pv
        LEFT JOIN piqure_rappel_user_vaccin AS puv
        ON pv.id = puv.id_vaccin
        WHERE puv.id = $getid";
$query = $pdo->prepare($sql);
$query->execute();
$vaccinuser = $query->fetchAll();

$sql = "SELECT id,name,content FROM piqure_rappel_vaccin ORDER BY name ASC";
$query = $pdo->prepare($sql);
$query->execute();
$getlistvaccins = $query->fetchall();

$effets = array(
    'oui' => 'oui',
    'non' => 'non'
);


if($vaccinuser[0]['secondary_effect'] == 1) {
    $vaccinuser[0]['secondary_effect']= 'oui';
}else{
    $vaccinuser[0]['secondary_effect']= 'non';
}
$errors= [];

if (!empty($_POST['submitted'])) {
    /*clean XSS*/
    $lot = cleanXss('lot');
    $lot = strtoupper($lot);
    $id_vaccin = cleanXss('vaccin');
    $effet = cleanXss('effet');
    if($effet == 'oui'){
        $effet = 1;
    }else{
        $effet = 0;
    }
    $comeffet = cleanXss('comeffet');

    if ($effet==1 and empty($comeffet)){
        $errors['comeffet']='Veuillez entrer un effet secondaire';
    }
    if ($effet==0 and !empty($comeffet)){
        $errors['comeffet']='Veuillez laisser ce champ vide';
    }
    /*validation*/
    $errors = validText($errors,$lot,'lot',5,20);


    if(count($errors)==0){
        $sql = "UPDATE piqure_rappel_user_vaccin SET id_vaccin = :id_vaccin,secondary_effect = :secondary,description_effect = :description,num_lot = :lot WHERE id = $getid";
        $query = $pdo->prepare($sql);
        $query->bindValue('id_vaccin', $id_vaccin, PDO::PARAM_INT);
        $query->bindValue('lot', $lot ,PDO::PARAM_STR);
        $query->bindValue('secondary', $effet, PDO::PARAM_STR);
        $query->bindValue('description', $comeffet, PDO::PARAM_STR);
        $query->execute();
        header("Location: gestionitemuser.php?id=". $vaccinuser[0]['id_user']);
    }

}








include('asset/inc/header.php'); ?>
<section id="gestion_vaccin">
    <div class="menu"> <a href="gestionitemuser.php?id=<?= $vaccinuser[0]['id_user'] ?>"><p>Coordonnée de l'Utilisateur</p></a></div>
    <div id="coordonnee_gestion" class="wrap2">
        <h1>Modification de vaccin </h1>
        <form action="" method="post" novalidate>

            <label for="vaccin">Choississez votre vaccin et sa pathologie</label>
            <select name="vaccin" id="vaccin">
                <?php foreach ($getlistvaccins as $key => $vaccin){
                    $idvaccin = $vaccin['id'];
                    $name= $vaccin['name']."(".$vaccin['content'].")"; ?>
                    <option value="<?php echo $idvaccin ; ?>" <?php if ($idvaccin == $vaccinuser[0]['id_vaccin']){echo 'selected';} ?>  > <?php echo $name ; ?></option>
                <?php } ?>
            </select>

            <label for="lot">N° de lot du vaccin</label>
            <input type="text" name="lot" id="lot" placeholder="N°lot" value="<?= $vaccinuser[0]['num_lot'] ?>">
            <span class="error"><?php viewError($errors,'lot'); ?></span>

            <label for="effet">Effets indésirables?</label>
            <select name="effet" id="effet">
                <?php foreach ($effets as $key => $effet){ ?>
                    <option value="<?php echo $key ?>" <?php if ($vaccinuser[0]['id_vaccin']==$effet) {echo 'selected';} ?>> <?php echo $effet ; ?></option>
                <?php } ?>
            </select>

            <label for="comeffet">A remplir si effets indésirables</label>
            <textarea name="comeffet" id="comeeffet"><?= $vaccinuser[0]['description_effect'] ?></textarea>
            <span class="error"><?php viewError($errors,'comeffet'); ?></span>


            <input type="submit" name="submitted" class="submitted" value="Ajouter vaccin">
        </form>
</section>
<?php include ('asset/inc/footer.php');
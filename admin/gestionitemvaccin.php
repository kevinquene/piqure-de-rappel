<?php
require('../asset/inc/pdo2.php');
require('../asset/inc/fonction.php');
require('asset/inc/statistique.php');
require('asset/inc/validation.php');
if (isBanned()){
    $_SESSION=array();
    header("Location: https://www.youtube.com/watch?v=dQw4w9WgXcQ");
}
if (!isAdmin()){
    header("Location: ../403.php");
}
$titre = 'Admin Gestion vaccins - PIQÛRE DE RAPPEL';
$getid =$_GET['id'];
$sql = "SELECT * FROM piqure_rappel_vaccin WHERE id = $getid ";
$query = $pdo->prepare($sql);
$query->execute();
$listitemvaccin = $query->fetchAll();

$rappel = explode(' ', $listitemvaccin[0]['rappel_day']);

$errors= [];

if(!empty($_POST['submitted'])){
    $name = cleanXss('name');
    $content = cleanXss('content');
    $rappel_number = cleanXss('rappel_number');
    $rappel_time = cleanXss('rappel_time');

    $errors = validText($errors, $name, 'name', 2, 100);
    $errors = validText($errors, $content, 'content', 2, 100);
    $errors = validText($errors, $rappel_number, 'rappel_number', 1, 3);
    $errors = validText($errors, $rappel_time, 'rappel_time', 1, 3);

    if(count($errors)==0){
        $id= $listitemvaccin[0]['id'];
        $totalrappel=$rappel_number.' '.$rappel_time;
        global $pdo;
        $sql = "UPDATE `piqure_rappel_vaccin` SET `name`=:name,`content`=:content,`rappel_day`=:rappel,`modified_at`=NOW() WHERE `id`= $id ";
        $query = $pdo->prepare($sql);
        $query->bindValue('name', $name, PDO::PARAM_STR);
        $query->bindValue('content', $content, PDO::PARAM_STR);
        $query->bindValue('rappel', $totalrappel, PDO::PARAM_STR);
        $query->execute();

        header("Location: gestionvaccin.php");
    }

}







include('asset/inc/header.php'); ?>
    <section id="gestion_vaccin">
        <div class="menu"> <a href="gestionvaccin.php"><p>Liste des Vaccins</p></a></div>
        <div class="wrap2">
            <h1>Donnée du <span class="nom"><?php  echo $listitemvaccin[0]['name'];?></span></h1>
            <div class="formulaire_inscription">
                <form action="" method="post" novalidate>

                    <label for="name">Nom du Vaccin <strong>*</strong></label>
                    <input type="text" name="name" id="name" placeholder="Ex: Pentavac®" value="<?php echo $listitemvaccin[0]['name']; ?>">
                    <span class="errors"><?php viewError($errors,'name'); ?></span>

                    <label for="content">Maladie <strong>*</strong></label>
                    <input type="text" name="content" id="content" placeholder="Ex: Tétanos" value="<?php echo $listitemvaccin[0]['content']; ?>">
                    <span class="errors"><?php viewError($errors,'content'); ?></span>

                    <label for="rappel_number">Nombre de Rappel <strong>*</strong></label>
                    <input type="number" name="rappel_number" id="rappel_number" placeholder="Ex: 2" value="<?php echo $rappel[0]; ?>">
                    <span class="errors"><?php viewError($errors,'rappel_number'); ?></span>

                    <label for="rappel_time">Temps entre deux Rappel en mois <strong>*</strong></label>
                    <input type="number" name="rappel_time" id="rappel_time" placeholder="Ex: 5" value="<?php echo $rappel[1]; ?>">
                    <span class="errors"><?php viewError($errors,'rappel_time'); ?></span>

                    <input type="submit" name="submitted" value="Modifier ce vaccin ">

                </form>
            </div>
        </div>
    </section>
<?php include ('asset/inc/footer.php');
?>


<script>
    function switchtable() {
        var insertContainer = document.getElementById('insertvaccin');
        var tableContainer = document.getElementById('tablevaccin');
        insertContainer.style.display = (insertContainer.style.display = 'none');
        tableContainer.style.display = (tableContainer.style.display = 'block');
    }

    function switchcreate() {
        var insertContainer = document.getElementById('insertvaccin');
        var tableContainer = document.getElementById('tablevaccin');
        tableContainer.style.display = (tableContainer.style.display = 'none');
        insertContainer.style.display = (insertContainer.style.display = 'block');
    }
</script>

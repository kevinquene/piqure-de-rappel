<?php
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css" integrity="sha512-z3gLpd7yknf1YoNbCzqRKc4qyor8gaKU1qmn+CShxbuBusANI9QpRohGBreCFkKxLhei6S9CQXFEbbKuqLg0DA==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <?php
    if(isset($titre)){ ?>
        <title><?php echo $titre ?></title>
    <?php }else{ ?>
        <title>PIQÛRE DE RAPPEL</title>
    <?php } ?>

    <link rel="stylesheet" href="asset/css/stylebootstrap.css">
    <link rel="stylesheet" href="asset/css/style.css">
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
    <script src="../asset/js/formatsecusocial.js"></script>

</head>
<body>
<div id="wrapper">
<header id="header">
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
            <li>
            <a href="../index.php">
                <div class="logo">
                    <a href="../index.php"><img src="../asset/img/Logo_principal_white.svg" alt="logo principal"></a>
                </div>
            </a>
            </li>

            <!-- Divider -->
            <li>
            <hr class="sidebar-divider my-0">
            </li>

            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="index.php">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span class="dash">Tableau de bord</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
                Gestion
            </div>

            <li class="nav-item">
                <a class="nav-link" href="gestionvaccin.php">
                    <i class="fas fa-fw fa-table"></i>
                    <span>Vaccins</span></a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="gestionutilisateur.php">
                    <i class="fas fa-fw fa-table"></i>
                    <span>Utilisateurs</span></a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="gestiondemande.php">
                    <i class="fas fa-fw fa-table"></i>
                    <span>Demandes</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
                Statistique
            </div>

            <!-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item">
                <a class="nav-link" href="vaccinstat.php">
                    <i class="fas fa-fw fa-chart-area"></i>
                    <span>Vaccins</span></a>
            </li>

            <!-- Nav Item - Charts -->
            <li class="nav-item">
                <a class="nav-link" href="userstat.php">
                    <i class="fas fa-fw fa-chart-area"></i>
                    <span>Utilisateurs</span></a>
            </li>
        </ul>

</header>

    <div class="boite-bouton-head">
    <button id="buton-header">
        >
    </button>
    </div>


    <script>
        let head = document.getElementById('header');
        let button = document.getElementById('buton-header');

        document.getElementById('buton-header').addEventListener('click', function () {
            if (head.style.zIndex === '100') {
                head.style.zIndex = '99';
                button.style.zIndex = '99';
                head.style.transform = 'translateX(0)';
                button.style.transform = 'translateX(0)';
            } else {
                head.style.zIndex = '100';
                button.style.zIndex = '100';
                head.style.transform = 'translateX(220px)';
                button.style.transform = 'translateX(220px)';
            }
        });
    </script>




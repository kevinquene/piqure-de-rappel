<?php
////////////////
/// clean XSS//
/// //////////
function cleanXss($key)
{
    return trim(strip_tags($_POST[$key]));
}
////////////////////////////////
/// recuperation valeur input//
/// //////////////////////////
function getPostValue($key, $data = '')
{
    if(!empty($_POST[$key]) ) {
        echo $_POST[$key];
    } elseif(!empty($data)) {
        echo $data;
    }
}
//////////////////////
/// renvoie erreur///
/// ////////////////
function viewError($err, $key)
{
    if(!empty($err[$key])) {
        echo $err[$key];
    }
}
///////////////////////
/// validation texte//
/// /////////////////
function validText($err, $value,$keyErr,$min,$max)
{
    if(!empty($value)) {
        if(mb_strlen($value) < $min ) {
            $err[$keyErr] = 'Veuillez renseigner au moins '.$min.' caractères';
        } elseif(mb_strlen($value) > $max ) {
            $err[$keyErr] = 'Veuillez ne pas renseigner plus de '.$max.' caractères';
        }
    } else {
        $err[$keyErr] = 'Veuillez renseigner ce champ';
    }
    return $err;
}
////////////////////////
/// validation Email///
/// //////////////////
function validEmail($err,$value, $keyErr)
{
    if(!empty($value)) {
        if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
            $err[$keyErr] = 'Veuillez renseigner un email valide';
        }
    } else {
        $err[$keyErr] = 'Veuillez renseigner un Email';
    }
    return $err;
}
////////////////////////////
/// validation connexion///
/// //////////////////////
function isLogged()
{
    if(!empty($_SESSION['user']['id'])){
        if($_SESSION['user']['id']){
            if(!empty($_SESSION['user']['email'])){
                if(!empty($_SESSION['user']['role'])){
                    if(!empty($_SESSION['user']['status'])){
                        if(!empty($_SESSION['user']['ip'])){
                            if($_SESSION['user']['ip'] == $_SERVER['REMOTE_ADDR']) {
                                return true;
                            }
                        }
                    }
                }
            }
        }
    }
    return false;
}
////////////////////////
/// validation admin///
/// //////////////////
function isAdmin(){
    if(isLogged()){
        if($_SESSION['user']['status'] == 'admin'){
            return true;
        }
    }
    return false;
}
////////////////////////
// validation banned///
/// //////////////////
function isBanned(){
    if(isLogged()){
        if($_SESSION['user']['status'] == 'banni'){
            return true;
        }
    }
    return false;
}
//////////////////////////
/// Validation select ///
////////////////////////

function validSelect($err, $value, $keyValue){
    if (empty($value)){
        $err[$keyValue] = 'Veuillez sélectionner un élément';
    }
    return $err;
}
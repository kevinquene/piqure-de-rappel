<?php

require('../../../asset/inc/pdo2.php');
require('../../../asset/inc/fonction.php');

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Récupérer les données envoyées par AJAX
    $id = $_POST['id'];

    // Assurez-vous de valider et sécuriser ces données avant de les utiliser dans votre requête SQL
    // (Vous pourriez utiliser des requêtes préparées par exemple)

    // Exemple d'utilisation de $key et $nouvelleVue pour exécuter une fonction PHP
    updateviewat($id);

    // Vous pouvez renvoyer une réponse au client si nécessaire
    echo 'Fonction PHP exécutée avec succès !';
} else {
    // Gérer les autres types de requêtes HTTP si nécessaire
    http_response_code(405);  // Méthode non autorisée
    echo 'Méthode non autorisée';
}

// Fonction PHP que vous souhaitez exécuter
function updateviewat($id) {
    global $pdo;
    $sql ="UPDATE `piqure_rappel_contact` SET `view_at`= NOW() WHERE `id` = :id";
    $query = $pdo->prepare($sql);
    $query->bindValue('id', $id, PDO::PARAM_INT);
    $query->execute();
}
<?php

$aujourdhui = date("Y-m-d");
$id18s=[];
$id1825s=[];
$id2535s=[];
$id3545s=[];
$id4555s=[];
$id55s=[];
$iduservac=[];
$arrayvaccins=[];


//Sql
$sql = "SELECT *
        FROM piqure_rappel_vaccin AS pv
        LEFT JOIN piqure_rappel_user_vaccin AS puv
        ON pv.id = puv.id_vaccin";
$query = $pdo->prepare($sql);
$query->execute();
$recupvaccin = $query->fetchAll();






$sql = "SELECT * FROM `piqure_rappel_user`";
$query = $pdo->prepare($sql);
$query->execute();
$users = $query->fetchAll();
$nbruser = count($users);

$sql = "SELECT * FROM `piqure_rappel_vaccin` ORDER BY id DESC";
$query = $pdo->prepare($sql);
$query->execute();
$vaccins = $query->fetchAll();

$sql = "SELECT * FROM `piqure_rappel_user_vaccin`";
$query = $pdo->prepare($sql);
$query->execute();
$usersvaccins = $query->fetchAll();
$nbruservaccin = count($usersvaccins);

$sql = "SELECT * FROM `piqure_rappel_user` WHERE role = 'inscrit'";
$query = $pdo->prepare($sql);
$query->execute();
$usersinscrit = $query->fetchAll();
$nbruserinscrit = count($usersinscrit);

if ($nbruser!=0 AND $nbruserinscrit!=0){
    $pourceninscrit = $nbruserinscrit/$nbruser;
    $pourceninscrit = $pourceninscrit*100;
    $pourceninscrit = round($pourceninscrit);
}else{
    $pourceninscrit=0;
}

$sql = "SELECT * FROM `piqure_rappel_contact` WHERE status = 'new'";
$query = $pdo->prepare($sql);
$query->execute();
$contactrequest = $query->fetchAll();
$nbrcontactrequest = count($contactrequest);

$sql = "SELECT * FROM `piqure_rappel_user` WHERE sex = 'Homme'";
$query = $pdo->prepare($sql);
$query->execute();
$maleusers = $query->fetchAll();
$nbrmaleusers = count($maleusers);

$sql = "SELECT * FROM `piqure_rappel_user` WHERE sex = 'Femme'";
$query = $pdo->prepare($sql);
$query->execute();
$femaleusers = $query->fetchAll();
$nbrfemaleusers = count($femaleusers);

$sql = "SELECT * FROM `piqure_rappel_user` WHERE sex = 'Autres'";
$query = $pdo->prepare($sql);
$query->execute();
$otherusers = $query->fetchAll();
$nbrotherusers = count($otherusers);

$sql = "SELECT * FROM `piqure_rappel_user` WHERE role != 'new' ORDER BY id DESC";
$query = $pdo->prepare($sql);
$query->execute();
$newusers = $query->fetchAll();


foreach ($usersvaccins as $uservac) {
    $iduservac[]=$uservac['id_user'];
}
$iduservaccounts = array_count_values($iduservac);
arsort($iduservaccounts);
$iduservaccountstop5 = array_slice($iduservaccounts, 0, 5, true);

foreach ($iduservaccountstop5 as $key=>$occurrence){
    $idusers[]=$key;
    $nbrusers[]=$occurrence;
    $prcusers[]=100*($occurrence/$nbrusers[0]);
}

$iduservaccountstop5Values = array_keys($iduservaccountstop5);
$iduservaccountstop5 = array_values($iduservaccountstop5);

foreach ($users as $user){
    if($user['id']==$iduservaccountstop5Values[0]){
        $user5['name'][0]=$user['name']. ' ' .$user['surname'];
        $user5['nbr'][0]=$iduservaccountstop5[0];
        $user5['prc'][0]=round($prcusers[0]);
    }elseif($user['id']==$iduservaccountstop5Values[1]){
        $user5['name'][1]=$user['name']. ' ' .$user['surname'];
        $user5['nbr'][1]=$iduservaccountstop5[1];
        $user5['prc'][1]=round($prcusers[1]);
    }elseif($user['id']==$iduservaccountstop5Values[2]){
        $user5['name'][2]=$user['name']. ' ' .$user['surname'];
        $user5['nbr'][2]=$iduservaccountstop5[2];
        $user5['prc'][2]=round($prcusers[2]);
    }elseif($user['id']==$iduservaccountstop5Values[3]){
        $user5['name'][3]=$user['name']. ' ' .$user['surname'];
        $user5['nbr'][3]=$iduservaccountstop5[3];
        $user5['prc'][3]=round($prcusers[3]);
    }elseif($user['id']==$iduservaccountstop5Values[4]){
        $user5['name'][4]=$user['name']. ' ' .$user['surname'];
        $user5['nbr'][4]=$iduservaccountstop5[4];
        $user5['prc'][4]=round($prcusers[4]);
    }
}


//Pour capter le nombre maximum de lettre du contenu des tableaux
foreach ($newusers as &$row) {
    foreach ($row as $key => &$value) {
        $row[$key] = truncateString($value);
    }
}



for ($i = 0; $i <= 9; $i++) {
    if (isset($vaccins[$i])){
        $tablevaccins[] = $vaccins[$i];
    }else{
        $tablevaccins[] = [
            'id' => 'X',
            'name' => 'X',
            'content' => 'X',
            'status' => 'X',
            'rappel_day' => 'X',
            'created_at' => 'X',
            'modified_at' => 'X',
            'created_by' => 'X',
            'modified_by' => 'X'
        ];
    }
}



//Pour trier id des users selon leur age
foreach ($users as $user){
    if (!empty($user['birthday'])){
        $dateNaissance = $user['birthday'];
        $diff = date_diff(date_create($dateNaissance), date_create($aujourdhui));
        $age = $diff->format('%y');
        if ($age<18){
            $id18s[] = $user['id'];
        }elseif($age<=25){
            $id1825s[] = $user['id'];
        }elseif($age<=35){
            $id2535s[] = $user['id'];
        }elseif($age<=45){
            $id3545s[] = $user['id'];
        }elseif($age<=55){
            $id4555s[] = $user['id'];
        }else{
            $id55s[] = $user['id'];
        }
    }
}

$nombre18 = count($id18s);
$nombre1825 = count($id1825s);
$nombre2535 = count($id2535s);
$nombre3545 = count($id3545s);
$nombre4555 = count($id4555s);
$nombre55 = count($id55s);


//Pour obtenir la moyenne de nombre de vaccination par age
$moyenvaccin18=calculmoyenvaccin($id18s, $usersvaccins);
$moyenvaccin1825=calculmoyenvaccin($id1825s, $usersvaccins);
$moyenvaccin2535=calculmoyenvaccin($id2535s, $usersvaccins);
$moyenvaccin3545=calculmoyenvaccin($id3545s, $usersvaccins);
$moyenvaccin4555=calculmoyenvaccin($id4555s, $usersvaccins);
$moyenvaccin55=calculmoyenvaccin($id55s, $usersvaccins);



//Pour recupérer le nom et le nombre de fois qu'on était fait chaque vaccin
foreach ($usersvaccins as $usersvaccin){
    $arrayvaccins[]=$usersvaccin['id_vaccin'];
}
$occurrences = array_count_values($arrayvaccins);
arsort($occurrences);
foreach ($occurrences as $key=>$occurrence){
    $idvaccins[]=$key;
    $nbrvaccins[]=$occurrence;
    $prcvaccins[]=100*($occurrence/$nbrvaccins[0]);
}
$idvaccins[]='';
$idvaccins[]='';
$idvaccins[]='';
$idvaccins[]='';
$idvaccins[]='';
$idvaccins[]='';

foreach ($vaccins as $vaccin){
    $vaccinname[$vaccin['id']] = $vaccin['name'];
}



//Pour comptez le nombre total de vaccination par tranche d'age
$total18=totalvaccinperage($id18s, $usersvaccins);
$total1825=totalvaccinperage($id1825s, $usersvaccins);
$total2535=totalvaccinperage($id2535s, $usersvaccins);
$total3545=totalvaccinperage($id3545s, $usersvaccins);
$total4555=totalvaccinperage($id4555s, $usersvaccins);
$total55=totalvaccinperage($id55s, $usersvaccins);



//Calcul de la part d'effet secondaire
$ysecondeffet=0;
$nsecondeffet=0;

foreach ($usersvaccins as $usersvaccin){
    if ($usersvaccin['secondary_effect'] == 1){
        $ysecondeffet = $ysecondeffet+1;
    }else{
        $nsecondeffet = $nsecondeffet+1;
    }
}

foreach ($tablevaccins as $key=>$tablevaccin) {
    $rappel[$key] = explode(' ', $tablevaccin['rappel_day']);
}



//Fonction
function truncateString($string, $length = 12) {
    if (strlen($string) > $length) {
        $string = substr($string, 0, $length - 3) . '...';
    }
    return $string;
}

function agecalcul($num){
    global $aujourdhui;
    global $newusers;
    if (!empty($newusers[$num]['birthday'])){
        $dateNaissance = $newusers[$num]['birthday'];
        $diff = date_diff(date_create($dateNaissance), date_create($aujourdhui));
        $returnage = $diff->format('%y');
        return $returnage;
    }
}


function calculmoyenvaccin($idvacs, $usersvaccins){
    $nbrs=calculnbrvaccin($idvacs, $usersvaccins);
    if(!empty($nbrs)){
        $sum = array_sum($nbrs);
        $moyenvaccin = $sum / count($nbrs);
        $moyenvaccin = round($moyenvaccin);
    }else{
        $moyenvaccin = 0;
    }
    return $moyenvaccin;
}

function calculnbrvaccin($idvacs, $usersvaccins){
    $nbrvac = 0;
    $nbrs[] = 0;
    foreach ($idvacs as $idvac) {
        foreach ($usersvaccins as $usersvaccin) {
            if ($usersvaccin['id_user'] == $idvac) {
                $nbrvac = $nbrvac + 1;
            }
        }
        if ($nbrvac != 0) {
            $nbrs[] = $nbrvac;
        }
    }return $nbrs;
}

function totalvaccinperage($ids, $usersvaccins){
    $total=0;
    $nbrs=calculnbrvaccin($ids, $usersvaccins);
    foreach ($nbrs as $nbr){
        $total=$total+$nbr;
    }
    return $total;
}

<!-- Courbe du nombre moyen de vaccins -->
<script>
    var ctxLine = document.getElementById('lineCanvas').getContext('2d');

    var lineChart = new Chart(ctxLine, {
    type: 'line',
    data: {
    labels: ['Moins de 18 ans', '18-25 ans', '25-35 ans', '35-45 ans', '45-55 ans', 'Plus de 55 ans'],
    datasets: [{
    data: [<?= $moyenvaccin18 ?>, <?= $moyenvaccin1825 ?>, <?= $moyenvaccin2535 ?>, <?= $moyenvaccin3545 ?>, <?= $moyenvaccin4555 ?>, <?= $moyenvaccin55 ?>],
    backgroundColor: 'rgba(75, 192, 192, 0.2)',
    borderColor: 'rgba(75, 192, 192, 1)',
    borderWidth: 2,
    pointBackgroundColor: 'rgba(75, 192, 192, 1)',
    showLine: true,
    fill: false,
    tension: 0.4
    }]
    },
        options: {
            maintainAspectRatio: false,
            layout: {
                padding: {
                    left: 10,
                    right: 25,
                    top: 25,
                    bottom: 0
                }
            },
            scales: {
                y: {
                    beginAtZero: true,
                    stepSize: 1,
                }
            },
            plugins: {
                legend: {
                    display: false
                }
            }
        }
    });
</script>


<!-- Cammembert du genre des utilisateur -->
<script>
    var ctxPie = document.getElementById('camembertCanvas').getContext('2d');

    var pieChart = new Chart(ctxPie, {
    type: 'pie',
    data: {
    labels: ['Hommes','Femmes','Autres'],
    datasets: [{
    data: [<?= $nbrmaleusers ?>, <?= $nbrfemaleusers ?>, <?= $nbrotherusers ?>],
    backgroundColor: ['#006eff', '#ff475a', '#e0f000'],
    borderColor: 'rgba(255, 255, 255, 1)',
    borderWidth: 2,
}]
},
    options: {
    plugins: {
    legend: {
    display: false
}
}
}
});

    ctxPie.canvas.style.width = '100%';
    ctxPie.canvas.style.height = 'auto';
</script>


<!-- Courbe du nombre total de vaccins -->
<script>
    var ctxLine = document.getElementById('lineCanvasTotal').getContext('2d');

    var lineChart = new Chart(ctxLine, {
        type: 'line',
        data: {
            labels: ['Moins de 18 ans', '18-25 ans', '25-35 ans', '35-45 ans', '45-50 ans', 'Plus de 50 ans'],
            datasets: [{
                data: [<?= $total18 ?>, <?= $total1825 ?>, <?= $total2535 ?>, <?= $total3545 ?>, <?= $total4555 ?>, <?= $total55 ?>],
                backgroundColor: 'rgba(75, 192, 192, 0.2)',
                borderColor: 'rgba(75, 192, 192, 1)',
                borderWidth: 2,
                pointBackgroundColor: 'rgba(75, 192, 192, 1)',
                showLine: true,
                fill: false,
                tension: 0.4
            }]
        },
        options: {
            maintainAspectRatio: false,
            layout: {
                padding: {
                    left: 10,
                    right: 25,
                    top: 25,
                    bottom: 0
                }
            },
            scales: {
                y: {
                    beginAtZero: true,
                    stepSize: 1,
                }
            },
            plugins: {
                legend: {
                    display: false
                }
            }
        }
    });
</script>


<!-- Cammembert des effets secondaires -->
<script>
    var ctxPie = document.getElementById('camemberteffect').getContext('2d');

    var pieChart = new Chart(ctxPie, {
        type: 'pie',
        data: {
            labels: ['Oui','Non',],
            datasets: [{
                data: [<?= $ysecondeffet ?>, <?= $nsecondeffet ?>],
                backgroundColor: ['red', 'green'],
                borderColor: 'rgba(255, 255, 255, 1)',
                borderWidth: 2,
            }]
        },
        options: {
            plugins: {
                legend: {
                    display: false
                }
            }
        }
    });

    ctxPie.canvas.style.width = '100%';
    ctxPie.canvas.style.height = 'auto';
</script>

<script>
    var ctxBar = document.getElementById('barCanvas').getContext('2d');

    var barChart = new Chart(ctxBar, {
        type: 'bar',
        data: {
            labels: ['Moins de 18 ans', '18-25 ans', '25-35 ans', '35-45 ans', '45-55 ans', 'Plus de 55 ans'],
            datasets: [{
                data: [<?= $nombre18 ?>, <?= $nombre1825 ?>, <?= $nombre2535 ?>, <?= $nombre3545 ?>, <?= $nombre4555 ?>, <?= $nombre55 ?>],
                backgroundColor: 'blue',
                borderColor: '#224abe',
                borderWidth: 2,
            }]
        },
        options: {
            maintainAspectRatio: false,
            layout: {
                padding: {
                    left: 10,
                    right: 25,
                    top: 25,
                    bottom: 0
                }
            },
            scales: {
                x: {
                    beginAtZero: true,
                    stepSize: 1,
                },
                y: {
                    beginAtZero: true,
                    stepSize: 1,
                }
            },
            plugins: {
                legend: {
                    display: false
                }
            }
        }
    });
</script>
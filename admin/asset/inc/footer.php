</div>
<footer>
    <div class="wrap">
        <div class="footer-logo">
            <div class="footer-logo-img"><img src="../asset/img/Logo_principal_white.svg" alt="footer logo"></div>
            <p class="footer-logo-text">Notre vocation, vous offrir un moyen de proteger votre avenir.</p>
        </div>

        <div class="footer-contact">
            <h2>Contactez nous</h2>
            <ul class="footer-bloc-contact">
                <li class="footer-adresse"><i class="fa-solid fa-location-dot"></i><a href="https://maps.app.goo.gl/6mvheCXRytScDUx59">10 Rue du Général Sarrail,<br> 76000 Rouen</a></li>
                <li class="footer-numero"><i class="fa-solid fa-phone"></i><a href="tel:0652417668">06 52 41 76 68</a></li>
                <li class="footer-mail"><i class="fa-solid fa-envelope"></i><a href="mailto:piqurederappelpro@outlook.fr">piqurederappelpro@outlook.fr</a></li>
            </ul>
        </div>

        <div class="footer-utils">
            <h2>Liens utiles</h2>
            <ul class="footer-bloc-utils">
                <li><a href="../conditiongenerale.php">Conditions Générales</a></li>
                <li><a href="../mentionlegale.php">Mentions légales</a></li>
                <li><a href="../politiqueconfidentialite.php">Politique de Confidentialité</a></li>
                <li><a href="../info.php">Plus d'informations</a></li>
            </ul>
        </div>
    </div>
    <div class="footer-copy">
        <p>© PIQÛRE DE RAPPEL 2023 <span>|</span><br> Created by Need4School</p>
    </div>
</footer>

</body>
</html>
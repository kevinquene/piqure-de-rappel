<?php
require('../asset/inc/pdo2.php');
require('../asset/inc/fonction.php');
require('asset/inc/statistique.php');
require('asset/inc/validation.php');
if (isBanned()){
    $_SESSION=array();
    header("Location: https://www.youtube.com/watch?v=dQw4w9WgXcQ");
}
if (!isAdmin()){
    header("Location: ../403.php");
}
$titre = 'Admin Gestion Utilisateurs - PIQÛRE DE RAPPEL';

global $pdo;
    $sql = "SELECT * FROM piqure_rappel_user WHERE role = 'inscrit' ORDER BY id ASC";
    $query = $pdo->prepare($sql);
    $query->execute();
    $listusers = $query->fetchAll();


include('asset/inc/header.php'); ?>
<section id="utilisateurs">
    <h1>Liste des Utilsateurs</h1>
    <table>
        <tr>
            <th>Nom, Prenom</th>
            <th>Status</th>
            <th>Genre</th>
        </tr> <?php
        foreach ($listusers as $listuser){?>
            <tr onclick="window.location='gestionitemuser.php?id=<?php echo $listuser['id']; ?>';" style="cursor: pointer">
                <td><?= $listuser['name'].', '.$listuser['surname'] ?></td><?php
                if ($listuser['status']=='no'){$listuser['status']='user';}
                ?><td><?= $listuser['status'] ?></td>
                <td><?= $listuser['sex'] ?></td>
            </tr>
        <?php }
        ?>
    </table>

</section>
<?php include ('asset/inc/footer.php');
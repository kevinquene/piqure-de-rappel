<?php
require('../asset/inc/pdo2.php');
require('../asset/inc/fonction.php');
require('asset/inc/validation.php');
if (isBanned()){
    $_SESSION=array();
    header("Location: https://www.youtube.com/watch?v=dQw4w9WgXcQ");
}
if (!isAdmin()){
    header("Location: ../403.php");
}
$titre = 'Admin Gestion utilisateurs - PIQÛRE DE RAPPEL';
$getid =$_GET['id'];
global $pdo;
$sql = "SELECT * FROM piqure_rappel_user WHERE id = $getid ";
$query = $pdo->prepare($sql);
$query->execute();
$listitemuser = $query->fetchAll();


$sql = "SELECT *
        FROM piqure_rappel_vaccin AS pv
        LEFT JOIN piqure_rappel_user_vaccin AS puv
        ON pv.id = puv.id_vaccin
        WHERE puv.id_user = $getid";
$query = $pdo->prepare($sql);
$query->execute();
$carnetusers = $query->fetchAll();
$carnetusers=array_reverse($carnetusers);


if($listitemuser[0]['status'] == 'no') {
    $listitemuser[0]['status'] = 'user';
}
$errors= [];
$genres = array(
    'Homme' => 'Homme',
    'Femme' => 'Femme',
    'Autres' => 'Autres'
);

$status=array(
        'user' => 'user',
        'admin'=>'admin',
        'banni'=>'banni'
);
$numSecuE = $listitemuser[0]['social_security'];
$numsecu=$numSecuE[0].' '.$numSecuE[1].$numSecuE[2].' '.$numSecuE[3].$numSecuE[4].' '.$numSecuE[5].$numSecuE[6].' '.$numSecuE[7].$numSecuE[8].$numSecuE[9].' '.$numSecuE[10].$numSecuE[11].$numSecuE[12];
$nommaj = strtoupper($listitemuser[0]["name"]." ".$listitemuser[0]["surname"]);

if(!empty($_POST['submitted'])){
    $email = cleanXss('email');
    $social_security = cleanXss('social_security');
    $social_security = str_replace(" ", "", $social_security);
    $getgenre = cleanXss('genres');
    $nom = cleanXss('nom');
    $prenom = cleanXss('prenom');
    $date = cleanXss('date');
    $tel = cleanXss('tel');
    $tel = str_replace(" ", "", $tel);
    $adresse = cleanXss('address') ;
    $getstatu = cleanXss('status');
    /*validations*/
    $errors = validEmail($errors, $email, 'email');
    $errors = validText($errors, $social_security, 'social_security', 13, 13);

    /*validation nom*/
    $errors = validText($errors,$nom,'nom',1,120);
    /*validation prenom*/
    $errors = validText($errors,$prenom,'prenom',1,120);
    /*validation date*/
    if (empty($_POST['date'])){
        $errors['date']= 'Veuillez ajouter votre date de naissance';
    }
    /*validation tel*/
    $errors = validText($errors,$tel,'tel',10,10);

    if(count($errors)==0){
    $id= $listitemuser[0]['id'];
        $sql = "UPDATE `piqure_rappel_user` SET `name`= :name,`surname`= :surname,`birthday`= :date,`sex`= :genre,`email`= :email,`phone_number`= :tel,`social_security`= :social_security,`status`= :status,`address`=:adresse,`modified_at`= NOW() WHERE id = $id";
        $query = $pdo->prepare($sql);
        $query->bindValue('name', $nom, PDO::PARAM_STR);
        $query->bindValue('email', $email, PDO::PARAM_STR);
        $query->bindValue('social_security', $social_security, PDO::PARAM_STR);
        $query->bindValue('surname', $prenom, PDO::PARAM_STR);
        $query->bindValue('date', $date);
        $query->bindValue('genre', $getgenre, PDO::PARAM_STR);
        $query->bindValue('adresse', $adresse, PDO::PARAM_STR);
        $query->bindValue('tel', $tel, PDO::PARAM_STR);
        $query->bindValue('status', $getstatu, PDO::PARAM_STR);
        $query->execute();

        header("Location: gestionutilisateur.php");
    }

}








include('asset/inc/header.php'); ?>
    <section id="infoitemuser">
        <div class="menu"> <div onclick="switchcoordo()"><p>Coordonnées</p></div> <div onclick="switchvaccins()"><p>Vaccins</p></div> </div>
        <div id="coordonnee_gestion" class="wrap2">
            <h1>Coordonnées de <span class="nom"><?php  echo $nommaj;?></span></h1>
            <div class="formulaire_inscription">
                <form action="" method="post" novalidate>

                    <label for="genres">Genre <strong>*</strong></label>
                    <select name="genres" id="genres">
                        <option value="<?php echo $listitemuser[0]['sex']; ?>"><?php echo $listitemuser[0]['sex']; ?></option>
                        <?php foreach ($genres as $key => $genre){
                            if ($genre != $listitemuser[0]['sex']){?>
                            <option value="<?php echo $key ?>" <?php if (!empty($_POST['genre']) && $_POST['genre'] === $key) {echo 'selected';} ?>> <?php echo $genre ; ?></option>
                        <?php }} ?>
                    </select>
                    <span class="errors"><?php viewError($errors,'genre'); ?></span>

                    <label for="social_security">N° Sécurité Sociale <strong>*</strong></label>
                    <input type="text" oninput="formatSecuSocial(this)" maxlength="19" name="social_security" id="social_security" placeholder="Ex: 1 23 45 67 890 123" value="<?php echo $numsecu; ?>">
                    <span class="errors"><?php viewError($errors,'social_security'); ?></span>

                    <label for="nom">Nom <strong>*</strong></label>
                    <input type="text" name="nom" id="nom" placeholder="Ex: Dupont" value="<?php echo $listitemuser[0]['name']; ?>">
                    <span class="errors"><?php viewError($errors,'nom'); ?></span>

                    <label for="prenom">Prénom <strong>*</strong></label>
                    <input type="text" name="prenom" id="prenom" placeholder="Ex: Michel" value="<?php echo $listitemuser[0]['surname']; ?>">
                    <span class="error"><?php viewError($errors,'prenom'); ?></span>

                    <label for="date">Date d'anniversaire <strong>*</strong></label>
                    <input type="date" name="date" id="date" value="<?php echo $listitemuser[0]['birthday']; ?>">
                    <span class="errors"><?php viewError($errors,'date'); ?></span>

                    <label for="email">E-mail <strong>*</strong></label>
                    <input type="email" name="email" id="email" placeholder="Ex: michel.dupont@gmail.com" value="<?php echo $listitemuser[0]['email']; ?>">
                    <span class="errors"><?php viewError($errors,'email'); ?></span>

                    <label for="tel">Numéro de téléphone <strong>*</strong></label>
                    <input type="text" name="tel" id="tel" oninput="formatNuemroTel(this)" maxlength="14" placeholder="Ex:07 12 34 56 78" value="<?php echo chunk_split($listitemuser[0]['phone_number'], 2, ' '); ?>">
                    <span class="errors"><?php viewError($errors,'tel'); ?></span>

                    <label for="address">Adresse</label>
                    <input type="text" name="address" id="address" value="<?php echo $listitemuser[0]['address']; ?>">
                    <span class="errors"><?php viewError($errors,'address'); ?></span>

                    <label for="status">Status<strong>*</strong></label>
                    <select name="status" id="status">
                        <option value="<?php echo $listitemuser[0]['status']; ?>"><?php echo $listitemuser[0]['status']; ?></option>
                        <?php foreach ($status as $key => $statu){
                            if($statu != $listitemuser[0]['status']){?>
                            <option value="<?php echo $key ?>" <?php if (!empty($_POST['statu']) && $_POST['statu'] === $key) {echo 'selected';} ?>> <?php  echo $statu ; ?></option>
                        <?php }} ?>
                    </select>

                    <input type="submit" name="submitted" value="Modifier cet utilisateur ">

                </form>
            </div>
        </div>
        <div class="wrap2" id="carnet_gestion">
            <h1>Vaccins de <span class="nom"><?php  echo $nommaj;?></span></h1>


            <table>
                <tr>
                    <th>Date de </br>vaccination</th>
                    <th>Vaccin & </br>(pathologie)</th>
                    <th>Effet </br>indésirable</th>
                    <th>N°lot</th>
                    <th>Rappel</th>
                </tr>
                <?php foreach ($carnetusers as  $key=>$itemvaccin) { ?>
                    <tr onclick="window.location='gestion2itemuser.php?id=<?php echo $itemvaccin['id']; ?>';" style="cursor: pointer">
                        <td><?php echo $itemvaccin['vaccin_at'];?></td>
                        <td><?php echo $itemvaccin['name'] . "</br>" . "(" . $itemvaccin['content'] . ")"; ?></td><?php
                        if (!empty($itemvaccin['description_effect'])){?>
                            <td><?php echo $itemvaccin['description_effect']; ?></td>
                        <?php }else{ ?>
                            <td><?php echo 'Aucun' ?></td>
                        <?php }
                        ?>
                        <td><?php echo $itemvaccin['num_lot']; ?></td>
                        <?php $rappel[$key] = explode(' ', $itemvaccin['rappel_day']);
                        if ($rappel[$key][1]!=0){?>
                            <td><?php echo $rappel[$key][1].' mois'; ?></td>
                        <?php }else{ ?>
                            <td><?php echo 'Aucun rappel'; ?></td>
                        <?php } ?>
                    </tr>
                <?php } ?>
            </table>
        </div>
    </section>
<?php include ('asset/inc/footer.php');
?>

<script>
    function switchcoordo() {
        var coordonnee = document.getElementById('coordonnee_gestion');
        var carnet = document.getElementById('carnet_gestion');
        carnet.style.display = (carnet.style.display = 'none');
        coordonnee.style.display = (coordonnee.style.display = 'block');
    }

    function switchvaccins() {
        var coordonnee = document.getElementById('coordonnee_gestion');
        var carnet = document.getElementById('carnet_gestion');
        coordonnee.style.display = (coordonnee.style.display = 'none');
        carnet.style.display = (carnet.style.display = 'block');
    }
</script>

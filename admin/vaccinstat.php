<?php
require('../asset/inc/pdo2.php');
require('../asset/inc/fonction.php');
require('asset/inc/statistique.php');
require('asset/inc/validation.php');
if (isBanned()){
    $_SESSION=array();
    header("Location: https://www.youtube.com/watch?v=dQw4w9WgXcQ");
}
if (!isAdmin()){
    header("Location: ../403.php");
}
$titre = 'Admin Statistique des Vaccins - PIQÛRE DE RAPPEL';
include('asset/inc/header.php');
?>

    <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800 grandtitrehauthome">Statistiques des Vaccins</h1>
        <p class="mb-4">Statistiques et données concernant les vacins, et surtout les vaccins pris par nos inscrits.</p>

        <!-- Content Row -->
        <div class="row">

            <div class="col-xl-8 col-lg-7">

                <!-- Area Chart -->
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Nombre Total de Vaccins Faits</h6>
                    </div>
                    <div class="card-body">
                        <div class="chart-area">
                            <canvas id="lineCanvasTotal"></canvas>
                        </div>
                        <hr>
                    </div>
                </div>

                <!-- Bar Chart -->
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Vaccins les Plus Faits</h6>
                    </div>
                    <div class="card-body">
                            <div class="card-body">
                                <?php if(isset($vaccinname[$idvaccins[0]])){ ?>
                                <h4 class="small font-weight-bold"><?= $vaccinname[$idvaccins[0]] ?> <span
                                            class="float-right"><?=$nbrvaccins[0]?></span></h4>
                                <div class="progress mb-4">
                                    <div class="progress-bar bg-danger" role="progressbar" style="width: <?= $prcvaccins[0] ?>%"
                                         aria-valuenow="<?= $nbrvaccins[0] ?>" aria-valuemin="0" aria-valuemax="<?= $nbrvaccins[0] ?>>"></div>
                                </div>
                                <?php }if(isset($vaccinname[$idvaccins[1]])){ ?>
                                <h4 class="small font-weight-bold"><?= $vaccinname[$idvaccins[1]] ?> <span
                                            class="float-right"><?=$nbrvaccins[1]?></span></h4>
                                <div class="progress mb-4">
                                    <div class="progress-bar bg-orange" role="progressbar" style="width: <?= $prcvaccins[1] ?>%"
                                         aria-valuenow="<?= $nbrvaccins[1] ?>" aria-valuemin="0" aria-valuemax="<?= $nbrvaccins[0] ?>"></div>
                                </div>
                                <?php }if(isset($vaccinname[$idvaccins[2]])){ ?>
                                <h4 class="small font-weight-bold"><?= $vaccinname[$idvaccins[2]] ?> <span
                                            class="float-right"><?=$nbrvaccins[2]?></span></h4>
                                <div class="progress mb-4">
                                    <div class="progress-bar bg-warning" role="progressbar" style="width: <?= $prcvaccins[2] ?>%"
                                         aria-valuenow="<?= $nbrvaccins[2] ?>" aria-valuemin="0" aria-valuemax="<?= $nbrvaccins[0] ?>"></div>
                                </div>
                                <?php }if(isset($vaccinname[$idvaccins[3]])){ ?>
                                <h4 class="small font-weight-bold"><?= $vaccinname[$idvaccins[3]] ?> <span
                                            class="float-right"><?=$nbrvaccins[3]?></span></h4>
                                <div class="progress mb-4">
                                    <div class="progress-bar " role="progressbar" style="width: <?= $prcvaccins[3] ?>%"
                                         aria-valuenow="<?= $nbrvaccins[3] ?>" aria-valuemin="0" aria-valuemax="<?= $nbrvaccins[0] ?>"></div>
                                </div>
                                <?php }if(isset($vaccinname[$idvaccins[4]])){ ?>
                                <h4 class="small font-weight-bold"><?= $vaccinname[$idvaccins[4]] ?> <span
                                            class="float-right"><?=$nbrvaccins[4]?></span></h4>
                                <div class="progress mb-4">
                                    <div class="progress-bar bg-info" role="progressbar" style="width: <?= $prcvaccins[4] ?>%"
                                         aria-valuenow="<?= $nbrvaccins[4] ?>" aria-valuemin="0" aria-valuemax="<?= $nbrvaccins[0] ?>"></div>
                                </div>
                                <?php }if(isset($vaccinname[$idvaccins[5]])){ ?>
                                <h4 class="small font-weight-bold"><?= $vaccinname[$idvaccins[5]] ?> <span
                                            class="float-right"><?=$nbrvaccins[5]?></span></h4>
                                <div class="progress mb-4">
                                    <div class="progress-bar bg-success" role="progressbar" style="width: <?= $prcvaccins[5] ?>%"
                                         aria-valuenow="<?= $nbrvaccins[5] ?>" aria-valuemin="0" aria-valuemax="<?= $nbrvaccins[0] ?>"></div>
                                </div>
                                <?php } ?>
                        </div>
                        <hr>
                    </div>
                </div>

                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Nombre Moyen de Vaccins Faits</h6>
                    </div>
                    <div class="card-body">
                        <div class="chart-area">
                            <canvas id="lineCanvas"></canvas>
                        </div>
                        <hr>
                    </div>
                </div>

            </div>

            <!-- Donut Chart -->
            <div class="col-xl-4 col-lg-5">
                <div class="card shadow mb-4">
                    <!-- Card Header - Dropdown -->
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Effect Secondaire</h6>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body">
                        <div class="card-body">
                            <div class="chart-pie pt-4 pb-2">
                                <canvas id="camemberteffect"></canvas>
                            </div>
                            <div class="mt-4 text-center small">
                                        <span class="mr-2">
                                            <i class="fa-solid fa-circle" style="color: red;"></i> Oui
                                        </span>
                                <span class="mr-2">
                                            <i class="fa-solid fa-circle" style="color: green;"></i> Non
                                        </span>
                            </div>
                        </div>
                        <hr>
                    </div>
                </div>
                <div class="card shadow mb-4">
                    <!-- Card Header - Dropdown -->
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Les derniers vaccins ajoutés</h6>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body marginfooter">
                        <div class="chart-pie pt-4">
                            <table>
                                <tr>
                                    <th class="vaccintable">Nom</th>
                                    <th class="vaccintable">Tps. de Rappel</th>
                                    <th class="vaccintable">Nbr. de Rappel</th>
                                </tr> <?php
                                foreach ($tablevaccins as $key=>$tablevaccin) { ?>
                                    <?php if (!empty($rappel[$key][1])){ ?>
                                    <tr>
                                        <td class="vaccintable"><?= $tablevaccin['name'] ?></td>
                                        <td class="vaccintable"><?= $rappel[$key][0]?></td>
                                        <td class="vaccintable"><?= $rappel[$key][1].' m.' ?></td>
                                    </tr>
                                <?php }}
                            ?> </table>
                        </div>
                        <hr>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>
<?php
require('asset/inc/diagramme.php');
include('asset/inc/footer.php');
?>
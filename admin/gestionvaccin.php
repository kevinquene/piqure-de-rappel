<?php
require('../asset/inc/pdo2.php');
require('../asset/inc/fonction.php');
require('asset/inc/validation.php');
if (isBanned()){
    $_SESSION=array();
    header("Location: https://www.youtube.com/watch?v=dQw4w9WgXcQ");
}
if (!isAdmin()){
    header("Location: ../403.php");
}
$titre = 'Admin Gestion des Vaccins - PIQÛRE DE RAPPEL';
$errors = array();

if (!empty($_POST['submitted'])){
    $namevax = cleanXss('name');
    $textvax = cleanXss('maladies');
    $rappelnumber = cleanXss('nbrrappel');
    $rappeltime = cleanXss('timerappel');
    if (!$rappeltime){
        if ($rappelnumber){
            $errors['rappel']='Veuillez rentrer les deux valeurs, ou n\'en rentrer aucune';
        }
    }
    if (!$rappelnumber){
        if ($rappeltime){
            $errors['rappel']='Veuillez rentrer les deux valeurs, ou n\'en rentrer aucune';
        }
    }

    $errors = validText($errors, $namevax, 'name', 2, 120);
    $errors = validText($errors, $textvax, 'maladies', 2, 300);


    if (count($errors) == 0){

        if ($rappelnumber){
            $rappels=$rappelnumber. ' ' .$rappeltime;
        }else{
            $rappels= '0 0';
        }
        $namevax=$namevax.'®';

        $sql ="INSERT INTO `piqure_rappel_vaccin`(`name`, `content`, `created_at`, `rappel_day`)
               VALUES (:name, :maladies, NOW(), :rappel)";
        $query = $pdo->prepare($sql);
        $query->bindValue('name', $namevax, PDO::PARAM_STR);
        $query->bindValue('maladies', $textvax, PDO::PARAM_STR);
        $query->bindValue('rappel', $rappels, PDO::PARAM_STR);
        $query->execute();
    }

}
$sql = "SELECT * FROM `piqure_rappel_vaccin` ORDER BY id ASC ";
$query = $pdo->prepare($sql);
$query->execute();
$vaccins = $query->fetchAll();

include('asset/inc/header.php');
?>

<section id="gestion_vaccin">
    <div class="menu"> <div onclick="switchtable()"><p>Liste des Vaccins</p></div> <div onclick="switchcreate()"><p>Ajouter un Vaccin</p></div> </div>
        <div id="insertvaccin" style="display: none">
        <h1>Insérer un Vaccin</h1>
        <div class="ajout_vaccin">
            <form action="" method="post" novalidate>

                <label for="name">Nom du Vaccin : </label>
                <input type="text" name="name" id="name" placeholder="Nom de votre vaccin" value="<?php getPostValue('name'); ?>">
                <span class="error"><?php viewError($errors, 'name'); ?></span>

                <textarea name="maladies" id="maladies" rows="10" cols="20" placeholder="Maladies concernées..."><?php getPostValue('maladies'); ?></textarea>
                <span class="error"><?php viewError($errors, 'maladies'); ?></span>

                <input type="number" name="nbrrappel" id="nbrrappel" placeholder="Nombre de rappel" value="<?php getPostValue('name'); ?>">

                <input type="number" name="timerappel" id="timerappel" placeholder="Temps entre chaque rappel en mois" value="<?php getPostValue('name'); ?>">
                <span class="error"><?php viewError($errors, 'rappel'); ?></span>

                <input type="submit" name="submitted" value="Ajouter un Vaccin">
            </form>
        </div>
        </div>
    <div id="tablevaccin">
        <h1>Liste des Vaccins</h1>
        <table>
            <tr>
                <th>Nom</th>
                <th>Maladie</th>
                <th class="rappel_colonne">Nombre de Rappel</th>
                <th class="rappel_colonne">Temps entre chaque rappel</th>
            </tr> <?php
                foreach ($vaccins as $key=>$vaccin){
                    $rappel[$key] = explode(' ', $vaccin['rappel_day']);
                    if ($rappel[$key][0]==100){$rappel[$key][0]='Rappels Réguliers';}?>
                    <tr onclick="window.location='gestionitemvaccin.php?id=<?php echo $vaccin['id']; ?>';" style="cursor: pointer">
                        <td><?= $vaccin['name'] ?></td>
                        <td><?= $vaccin['content'] ?></td>
                        <td class="rappel_colonne"><?= $rappel[$key][0] ?></td>
                        <td class="rappel_colonne"><?= $rappel[$key][1].' mois' ?></td>
                    </tr>
                <?php }
            ?>
        </table>
    </div>
</section>
</div>
</div>
<?php
include ('asset/inc/footer.php');
?>

<script>
    function switchtable() {
        var insertContainer = document.getElementById('insertvaccin');
        var tableContainer = document.getElementById('tablevaccin');
        insertContainer.style.display = (insertContainer.style.display = 'none');
        tableContainer.style.display = (tableContainer.style.display = 'block');
    }

    function switchcreate() {
        var insertContainer = document.getElementById('insertvaccin');
        var tableContainer = document.getElementById('tablevaccin');
        tableContainer.style.display = (tableContainer.style.display = 'none');
        insertContainer.style.display = (insertContainer.style.display = 'block');
    }
</script>

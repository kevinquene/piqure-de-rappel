<?php
require('../asset/inc/pdo2.php');
require('../asset/inc/fonction.php');
require('asset/inc/validation.php');
if (isBanned()){
    $_SESSION=array();
    header("Location: https://www.youtube.com/watch?v=dQw4w9WgXcQ");
}
if (!isAdmin()){
    header("Location: ../403.php");
}
$titre = 'Admin Gestion des demandes - PIQÛRE DE RAPPEL';
$errors= [];


$sql = "SELECT * FROM `piqure_rappel_contact` WHERE status = 'new' ORDER BY id DESC ";
$query = $pdo->prepare($sql);
$query->execute();
$new_requests = $query->fetchAll();

$sql = "SELECT * FROM `piqure_rappel_contact` WHERE status = 'resolved' ORDER BY id DESC ";
$query = $pdo->prepare($sql);
$query->execute();
$old_requests = $query->fetchAll();

$sql = "SELECT * FROM `piqure_rappel_user`";
$query = $pdo->prepare($sql);
$query->execute();
$users = $query->fetchAll();

foreach ($users as $user){
    foreach ($new_requests as $key=>$new_request){
        if ($new_request['id_user'] == $user['id']){
            $new_request_tris[$key]['id'] = $new_request['id'];
            $new_request_tris[$key]['name'] = $user['name']. ' ' .$user['surname'];
            $new_request_tris[$key]['title'] = $new_request['object'];
            $new_request_tris[$key]['content'] = $new_request['content'];
            $new_request_tris[$key]['date'] = $new_request['created_at'];
            $new_request_tris[$key]['vu'] = $new_request['view_at'];
        }
    }

    foreach ($old_requests as $key=>$old_request){
        if ($old_request['id_user'] == $user['id']){
            $old_request_tris[$key]['name'] = $user['name']. ' ' .$user['surname'];
            $old_request_tris[$key]['title'] = $old_request['object'];
            $old_request_tris[$key]['content'] = $old_request['content'];
            $old_request_tris[$key]['date'] = $old_request['answer_at'];
            $old_request_tris[$key]['answer'] = $old_request['answer_admin'];
        }
    }
}

if (!empty($_POST['sub'])){
    $textrep = cleanXss('reponse_requete');
    $id = cleanXss('id');

    $errors = validText($errors, $textrep, 'reponse_requete', 2, 1200);

    if (count($errors) == 0){
        debug($errors);
        $sql ="UPDATE `piqure_rappel_contact` SET `answer_admin`=:rep,`answer_at`= NOW(), `status`='resolved' WHERE `id` = :id";
        $query = $pdo->prepare($sql);
        $query->bindValue('id', $id, PDO::PARAM_INT);
        $query->bindValue('rep', $textrep, PDO::PARAM_STR);
        $query->execute();
        header("Location: gestiondemande.php");
    }

}

include('asset/inc/header.php');
?>

    <section id="gestion_demande">
        <h1>Gestion des Demandes</h1>
        <div class="total_demande">
            <h2>Demandes en Attente</h2>
            <?php
            if (!empty($new_request_tris)){
            foreach ($new_request_tris as $key=>$new_request_tri) {
                echo '<div class="menudemande" onclick="toggleTableNew(' . $key . ', \'' . $new_request_tri['vu'] . '\', ' . $new_request_tri['id'] . ')"><strong>'. $new_request_tri['title'] .'<div class="menudate">'. $new_request_tri['date'] .'</strong></div></div>';
                echo '<div id="tableContainernew' . $key . '" class="tableContainer">';
                echo '<table>';
                echo '<thead>';
                echo '<tr>';
                echo '<th class="usertable">Utilisateurs</th>';
                echo '<th class="contentable">Requête</th>';
                echo '</tr>';
                echo '</thead>';
                echo '<tbody>';
                echo '<tr>';
                echo '<td>' . $new_request_tri['name'] . '</td>';
                echo '<td>' . nl2br($new_request_tri['content']) . '</td>';
                echo '</tr>';
                echo '</tbody>';
                echo '</table>';
                echo '<form action="" class="formdemande" method="post" novalidate>';
                echo '<textarea name="reponse_requete" class="reponse_requete" placeholder="Votre réponse"></textarea>';
                echo '<input type="number" class="id" name="id" value="'.$new_request_tri['id'].'">';
                echo '<input type="submit" class="sub" name="sub" value="Envoyer ma réponse">';
                echo '</form>';
                echo '</div>';
            }}else{
                echo '<div class="menudemande"';
                echo '<div class="tableContainer">';
                echo 'Vous n\'avez aucune demande en attente';
                echo '</div>';
            }
            ?>

            <h2>Demandes Résolu</h2>
            <?php
            if (!empty($old_request_tris)){
            foreach ($old_request_tris as $key=>$old_request_tri) {
                echo '<div class="menudemande" onclick="toggleTableResolv(' . $key . ')"><strong>'. $old_request_tri['title'] .'<div class="menudate">'. $old_request_tri['date'] .'</strong></div></div>';
                echo '<div id="tableContainerold' . $key . '" class="tableContainer">';
                echo '<table>';
                echo '<thead>';
                echo '<tr>';
                echo '<th class="usertable">Utilisateurs</th>';
                echo '<th class="contentable">Requète</th>';
                echo '</tr>';
                echo '</thead>';
                echo '<tbody>';
                echo '<tr>';
                echo '<td>' . $old_request_tri['name'] . '</td>';
                echo '<td>' . nl2br($old_request_tri['content']) . '</td>';
                echo '</tr>';
                echo '<tr>';
                echo '<td colspan="2"> <strong>Réponse: </strong> ' . nl2br($old_request_tri['answer']) . '</td>';
                echo '</tr>';
                echo '</tbody>';
                echo '</table>';
                echo '</div>';
            }}else{
                echo '<div class="menudemande"';
                echo '<div class="tableContainer">';
                echo 'Vous n\'avez aucune demande résolu';
                echo '</div>';
            }
            ?>
        </div>
    </section>
    </div>
    </div>
<?php
include ('asset/inc/footer.php');
?>

<script>
        function toggleTableNew(key, view, id) {
            if (!view){
                $.ajax({
                    type: 'POST',
                    url: 'asset/inc/execute_php_function.php',  // Assurez-vous d'ajuster le chemin vers votre fichier PHP
                    data: { id: id },
                    success: function(response) {
                        console.log(response);
                    },
                    error: function(error) {
                        console.error(error);
                    }
                });
            }
            var tableContainer = document.getElementById('tableContainernew' + key);
            tableContainer.style.display = (tableContainer.style.display === 'none' || tableContainer.style.display === '') ? 'block' : 'none';
        }

        function toggleTableResolv(key) {
            var tableContainer = document.getElementById('tableContainerold' + key);
            tableContainer.style.display = (tableContainer.style.display === 'none' || tableContainer.style.display === '') ? 'block' : 'none';
        }

        function reload(){
            location.reload();
        }
</script>
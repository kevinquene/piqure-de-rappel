<?php
require('asset/inc/pdo2.php');
require('asset/inc/fonction.php');
require('asset/inc/validation.php');
if (isBanned()){
    $_SESSION=array();
    header("Location: https://www.youtube.com/watch?v=dQw4w9WgXcQ");
}

if (isLogged()){
    $user = $_SESSION['user']['id'];
    header("Location: moncarnet_index.php?id=$user");
}
$titre = 'Changement de Mot de Passe - PIQÛRE DE RAPPEL';
$errors = array();
$success = false ;

if (!empty($_GET['tokens'])){
    $sql = "SELECT * FROM `piqure_rappel_user` WHERE tokens = :token";
    $query = $pdo->prepare($sql);
    $query->bindValue('token', $_GET['tokens'], PDO::PARAM_STR);
    $query->execute();
    $users = $query->fetch();
    if(!empty($users)) {
        if (!empty($_POST['submitted'])) {
            $password = cleanXss('password');
            $password2 = cleanXss('password2');

            $errors = validText($errors, $password, 'password', 6, 20);
            if (empty($password2)) {
                $errors['password2'] = 'Veuillez renseigner ce champ';
            }
            if (empty($errors['password'])) {
                if (!empty($password) && !empty($password2)) {
                    if ($password != $password2) {
                        $errors['password'] = 'Vos mots de passe sont différents.';
                    }
                }
            }

            if (empty($errors)) {
                $ancientoken = $_GET['tokens'];
                $hashPassword = password_hash($password, PASSWORD_DEFAULT);
                $tokens = generate_random_string(130);
                $success = true;

                $sql ="UPDATE `piqure_rappel_user` SET `tokens`= :tokens,`password`= :hashPassword WHERE `tokens` = :ancientoken";
                $query = $pdo->prepare($sql);
                $query->bindValue('hashPassword', $hashPassword, PDO::PARAM_STR);
                $query->bindValue('ancientoken', $ancientoken, PDO::PARAM_STR);
                $query->bindValue('tokens', $tokens, PDO::PARAM_STR);
                $query->execute();

                header('Location: connexion.php');
            }
        }
    }else{
        header('Location: 404.php');
    }
}else{
    header('Location: 404.php');
}

include('asset/inc/header.php'); ?>


    <section id="changemdp">
        <div class="wrap1">

            <div class="bloc-changemdp">
                <h1>Veuillez entrer votre <br>nouveau mot de passe</h1>
                <form action="" method="post" novalidate>
                    <label for="password">Créez votre Mot-de-passe <strong>*</strong></label>
                    <div class="password-container">
                        <input id="password" name="password" type="password" value="<?php getPostValue('password'); ?>" required>
                        <span id="togglePassword" onclick="togglePasswordVisibility()"><img id="togglePassword_img" src="asset/img/oeil_ouvert.png" style="width: 35px; margin-top: 20px; margin-right: 10px" alt="oeil_mdp"></span>
                    </div>
                    <span class="errors"><?php viewError($errors,'password'); ?></span><br>

                    <label for="password2">Confirmez votre Mot-de-passe <strong>*</strong></label>
                    <div class="password-container">
                        <input id="password2" name="password2" type="password" value="" required>
                        <span id="togglePassword" onclick="togglePassword2Visibility()"><img id="togglePassword2_img" src="asset/img/oeil_ouvert.png" style="width: 35px; margin-top: 20px; margin-right: 10px" alt="oeil_mdp"></span>
                    </div>
                    <span class="errors"><?php viewError($errors,'password2'); ?></span>

                    <input type="submit" name="submitted" class="sub" value="VALIDER">
                </form>
            </div>
        </div>
    </section>

<?php
include ('asset/inc/footer.php');
?>


<script>
    function togglePasswordVisibility() {
        var passwordInput = document.getElementById("password");
        var toggleIcon = document.getElementById("togglePassword_img");

        if (passwordInput.type === "password") {
            passwordInput.type = "text";
            toggleIcon.setAttribute("src", "asset/img/oeil_ferme.png");
        } else {
            passwordInput.type = "password";
            toggleIcon.setAttribute("src", "asset/img/oeil_ouvert.png");
        }
    }
</script>

<script>
    function togglePassword2Visibility() {
        var passwordInput = document.getElementById("password2");
        var toggleIcon = document.getElementById("togglePassword2_img");

        if (passwordInput.type === "password") {
            passwordInput.type = "text";
            toggleIcon.setAttribute("src", "asset/img/oeil_ferme.png");
        } else {
            passwordInput.type = "password";
            toggleIcon.setAttribute("src", "asset/img/oeil_ouvert.png");
        }
    }
</script>

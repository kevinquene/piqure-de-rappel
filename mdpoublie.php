<?php
require('asset/inc/pdo2.php');
require('asset/inc/fonction.php');
require('asset/inc/validation.php');
require('asset/inc/chemin-mail-mdp.php');

$titre = 'Mot de Passe Oublié - PIQÛRE DE RAPPEL';
if (isBanned()){
    $_SESSION=array();
    header("Location: https://www.youtube.com/watch?v=dQw4w9WgXcQ");
}
if (isLogged()){
    $user = $_SESSION['user']['id'];
    header("Location: moncarnet_index.php?id=$user");
}
$errors = [];
$success = false;

if (!empty($_POST['submitted'])) {
    $email = cleanXss('email-mdpoublie');
    $errors = validEmail($errors, $email, 'mdpoublie');

    if (empty($errors['mdpoublie'])) {
        $sql = "SELECT * FROM piqure_rappel_user WHERE email = :email";
        $query = $pdo->prepare($sql);
        $query->bindValue('email', $email);
        $query->execute();
        $verifEmail = $query->fetch();
        if (!empty($verifEmail)) {
            $_SESSION['recupmdp'] = array(
                'email' => $verifEmail['email'],
                'chemin' => $chemin,
                'tokens' => $verifEmail['tokens'],
                'antispam' => false
            );
            header('Location: succesmail.php');
        }else{
            $errors['mdpoublie'] = "Cet Email n'est pas enregistré";
        }
    }
}


include('asset/inc/header.php');
?>

<section id="mdpoublie">
    <div class="wrap1">

        <div class="bloc-mdpoublie">
        <h1>VOUS AVEZ OUBLIÉ <br>VOTRE MOT DE <br class="br2">PASSE ?</h1>
        <form action="" method="post" novalidate>
            <input id="email-mdpoublie" name="email-mdpoublie" type="text" placeholder="Adresse Email" value="" required>
            <span class="error"><br><?php if (!empty($errors['mdpoublie'])){echo $errors['mdpoublie'];} ?></span>

            <input class="sub" type="submit" name="submitted" value="Réinitialiser le mot de passe">
        </form>
        <div class="oublie-mdp-retour-buton"><a href="connexion.php">Retour à Connexion</a></div>
        </div>
    </div>
</section>

<?php

include ('asset/inc/footer.php');
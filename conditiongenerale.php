<?php
require('asset/inc/pdo2.php');
require('asset/inc/fonction.php');
require('asset/inc/validation.php');

if (isBanned()){
    $_SESSION=array();
    header("Location: https://www.youtube.com/watch?v=dQw4w9WgXcQ");
}

$titre = 'Conditions Générales - PIQÛRE DE RAPPEL';


include('asset/inc/header.php'); ?>
<section id="cgu">
    <div class="wrapcontenu">
        <h1>CONDITIONS GÉNÉRALES D'UTILISATION (CGU)</h1>

        <h3>1. Acceptation des Conditions : </h3>
        <p>- En utilisant ce service, vous acceptez de vous conformer à ces <span class="strong">Conditions Générales d'Utilisation</span> . Si vous n'acceptez pas ces CGU, veuillez ne pas utiliser le Service.</p>

        <h3>2. Modifications des CGU : </h3>
        <p>- Nous nous réservons le droit de modifier ces CGU à tout moment. Les modifications prendront effet dès leur publication sur le Service. Il est de votre responsabilité de consulter régulièrement les CGU pour rester informé des mises à jour. En continuant à utiliser le Service après la publication des modifications, <span class="strong">vous acceptez les CGU révisées</span> .</p>

        <h3>3. Utilisation du Service : </h3>
        <p>- Vous acceptez d'utiliser le Service conformément à toutes les lois et réglementations applicables.</p>
        <p>- Vous acceptez de ne pas utiliser le Service à des fins illégales, offensantes, diffamatoires, ou pour violer les droits de tiers.</p>
        <p>- Vous acceptez de ne pas accéder au Service de manière non autorisée ou de tenter de compromettre la sécurité du Service.</p>

        <h3>4. Contenu de l'Utilisateur : </h3>
        <p>- <span class="strong">Vous êtes responsable</span> de tout contenu que vous publiez, téléchargez ou partagez sur le Service. Vous garantissez que vous avez le droit de publier ce contenu et que cela ne viole pas les droits de tiers.</p>
        <p>- Nous nous réservons le droit de supprimer tout contenu que nous considérons inapproprié ou en violation des CGU.</p>

        <h3>5. Propriété Intellectuelle : </h3>
        <p>- Tous les contenus, marques de commerce, logos et droits de <span class="strong">propriété intellectuelle</span> associés au Service sont la propriété de <span class="strong">Piqûre de Rappel</span>.</p>
        <p>- Vous n'avez pas le droit de copier, reproduire, distribuer ou utiliser ces éléments sans autorisation.</p>

        <h3>6. Confidentialité : </h3>
        <p>- Notre <strong class="strong">politique de confidentialité </strong> régit la manière dont nous recueillons, utilisons et stockons vos données personnelles. En utilisant le Service, vous acceptez notre politique de confidentialité.</p>

        <h3>7. Limitation de Responsabilité : </h3>
        <p>- Nous nous efforçons de maintenir le Service accessible et sûr, mais nous ne garantissons pas sa disponibilité continue ou son absence de bugs ou d'erreurs.</p>
        <p>- En aucun cas, <span class="strong">Piqûre De Rappel</span> ne sera responsable des dommages indirects, spéciaux, consécutifs ou exemplaires résultant de l'utilisation du Service.</p>

        <h3>8. Résiliation : </h3>
        <p>- Nous nous réservons le droit de <span class="strong">résilier</span>  ou de <span class="strong">suspendre votre accès</span>  au Service en cas de violation des CGU.</p>

        <h3>9. Loi Applicable et Juridiction : </h3>
        <p>- Ces CGU sont régies par les <span class="strong">lois du règlement général sur la protection des données</span> - RGPD, et tout litige découlant de ces CGU sera soumis à la juridiction exclusive des tribunaux du Le règlement général sur la protection des données - RGPD.</p>

        <h3>10. Contact : </h3>
        <p>- Si vous avez des questions ou des préoccupations concernant ces CGU, veuillez nous contacter à <span class="strong">10 rue du Général Sarrail 76000 ROUEN</span> .</p>
    </div>
</section>
<?php include ('asset/inc/footer.php');
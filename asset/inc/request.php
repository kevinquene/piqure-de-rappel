<?php

//////////////////////////////////
/// different types de requete///
/// ////////////////////////////
function ajoutvaccin($user,$id_vaccin,$effet,$comeffet,$lot)
{

    global $pdo;
    $sql = "INSERT INTO `piqure_rappel_user_vaccin` (`id_user`, `id_vaccin`, `secondary_effect`, `description_effect`, `created_at`, `vaccin_at`, `num_lot`) 
                VALUES (:id_user,:id_vaccin,:effect,:description,NOW(),NOW(),:lot)";
    $query = $pdo->prepare($sql);
    $query->bindValue('id_user', $user, PDO::PARAM_INT);
    $query->bindValue('id_vaccin', $id_vaccin ,PDO::PARAM_INT);
    $query->bindValue('effect', $effet, PDO::PARAM_STR);
    $query->bindValue('description', $comeffet, PDO::PARAM_STR);
    $query->bindValue('lot', $lot, PDO::PARAM_STR);
    $query->execute();
    header("Location: moncarnet_index.php?id=$user");
}

function updateinscription($nom,$prenom,$date,$getgenre,$tel,$adresse,$id)
{   $inscrit='inscrit';
    $id=$_GET['id'];
    global $pdo;
    $sql = "UPDATE piqure_rappel_user SET name = :name,surname = :surname,birthday = :date,sex = :genre,phone_number = :tel,role = :inscrit,modified_at = NOW(),address = :adresse WHERE id = :id";
    $query = $pdo->prepare($sql);
    $query->bindValue('name', $nom, PDO::PARAM_STR);
    $query->bindValue('surname', $prenom ,PDO::PARAM_STR);
    $query->bindValue('date', $date );
    $query->bindValue('genre', $getgenre, PDO::PARAM_STR);
    $query->bindValue('tel', $tel, PDO::PARAM_STR);
    $query->bindValue('adresse',$adresse, PDO::PARAM_STR);
    $query->bindValue('inscrit',$inscrit, PDO::PARAM_STR);
    $query->bindValue('id',$id, PDO::PARAM_INT);
    $query->execute();



    $_SESSION['user']['role'] = 'inscrit';

    header("Location: moncarnet_index.php?id=$id");
}
function modifcoordonnee($newtel,$newadresse,$newpostal,$newville,$id)
{   $inscrit='inscrit';
    $id=$_GET['id'];
    global $pdo;
    $sql = "UPDATE piqure_rappel_user SET newtel = :tel, newrue = :rue, newpostal = :postal, newville = :ville, modified_at = NOW() WHERE id = :id";
    $query = $pdo->prepare($sql);
    $query->bindValue('tel', $newtel, PDO::PARAM_STR);
    $query->bindValue('adresse', $newadresse ,PDO::PARAM_STR);
    $query->bindValue('postal', $newpostal, PDO::PARAM_STR );
    $query->bindValue('ville', $newville, PDO::PARAM_STR);
    $query->bindValue('inscrit',$inscrit, PDO::PARAM_STR);
    $query->bindValue('id',$id, PDO::PARAM_INT);
    $query->execute();



    $_SESSION['user']['role'] = 'inscrit';

    header("Location: moncarnet_index.php?id=$id");
}

function updatevaccin($nom,$prenom,$date,$getgenre,$tel,$adresse,$id)
{   $inscrit='inscrit';
    $id=$_GET['id'];
    global $pdo;
    $sql = "UPDATE piqure_rappel_user SET name = :name,surname = :surname,birthday = :date,sex = :genre,phone_number = :tel,role = :inscrit,modified_at = NOW(),address = :adresse WHERE id = :id";
    $query = $pdo->prepare($sql);
    $query->bindValue('name', $nom, PDO::PARAM_STR);
    $query->bindValue('surname', $prenom ,PDO::PARAM_STR);
    $query->bindValue('date', $date );
    $query->bindValue('genre', $getgenre, PDO::PARAM_STR);
    $query->bindValue('tel', $tel, PDO::PARAM_STR);
    $query->bindValue('adresse',$adresse, PDO::PARAM_STR);
    $query->bindValue('inscrit',$inscrit, PDO::PARAM_STR);
    $query->bindValue('id',$id, PDO::PARAM_INT);
    $query->execute();


    $_SESSION['user']['role'] = 'inscrit';
    header("Location: moncarnet_index.php?id=$id");
}
//function updateinscription($name,$surname,$birthday,$sex,$tel,$role,$adress)
//{
//    global $pdo;
//    $sql = "UPDATE blog_articles SET title = :titre,content = :contenu,auteur = :auteur,modified_at = NOW(),status = :status WHERE id = :id";
//    $query = $pdo->prepare($sql);
//    $query->bindValue('titre', $titre, PDO::PARAM_STR);
//    $query->bindValue('contenu', $content ,PDO::PARAM_STR);
//    $query->bindValue('auteur', $auteur, PDO::PARAM_STR);
//    $query->bindValue('status', $status, PDO::PARAM_STR);
//    $query->bindValue('id',$id, PDO::PARAM_INT);
//    $query->execute();
//}

//function updateinscription($id,$titre,$content,$auteur,$status)
//{

//   global $pdo;
//   $sql = "SELECT * FROM piqure_rappel_user WHERE ID = :id";
//$query = $pdo->prepare($sql);
//   $query->bindValue('id',$id, PDO::PARAM_INT);
//   $query->execute();
//  return $query->fetch();
//}

function getAllPost() {
    global $pdo;
    $sql = "SELECT * FROM blog_articles";
    $query = $pdo->prepare($sql);
    $query->execute();
    return $query->fetchAll();
}
function getArticleById($id) {
    global $pdo;
    $sql = "SELECT * FROM blog_articles WHERE ID = :id";
    $query = $pdo->prepare($sql);
    $query->bindValue('id',$id, PDO::PARAM_INT);
    $query->execute();
    return $query->fetch();
}


function getAllPostByStatus($status = 'publish') {
    global $pdo;
    $sql = "SELECT * FROM blog_articles WHERE status = '$status' ORDER BY created_at DESC";
    $query = $pdo->prepare($sql);
    $query->execute();
    return $query->fetchAll();
}




function insertComment($id,$auteur,$contenu)
{
    global $pdo;
    $sql = "INSERT INTO blog_comments (id_article,content, auteur,created_at,status) 
                VALUES (:id,:contenu,:auteur,NOW(),'new')";
    $query = $pdo->prepare($sql);
    $query->bindValue('contenu', $contenu ,PDO::PARAM_STR);
    $query->bindValue('auteur', $auteur, PDO::PARAM_STR);
    $query->bindValue('id', $id, PDO::PARAM_STR);
    $query->execute();
    return $pdo->lastInsertId();
}


function getAllCommentForThisArticle($id_article, $status = 'publish')
{
    global $pdo;
    $sql = "SELECT * FROM blog_comments WHERE id_article = :id AND status = '$status' ORDER BY created_at DESC";
    $query = $pdo->prepare($sql);
    $query->bindValue('id',$id_article, PDO::PARAM_INT);
    $query->execute();
    return $query->fetchAll();
}


//function getAllCommentsByStatus($status = 'publish')
//{
//  global $pdo;
// $sql = "SELECT a.title, c.id, c.auteur, c.content, c.created_at FROM blog_comments AS c
//           LEFT JOIN blog_articles AS a ON a.id = c.id_article
//           WHERE c.status = '$status'";
//   $query = $pdo->prepare($sql);
//    $query->execute();
//  return $query->fetchAll();
//}

//////////////
/// delete///
/// ////////
//function getCommentById($id) {
//   global $pdo;
//   $sql = "SELECT * FROM blog_comments WHERE ID = :id";
///$query = $pdo->prepare($sql);
//   $query->bindValue('id',$id, PDO::PARAM_INT);
//   $query->execute();
//   return $query->fetch();
//}

//if(!empty($_GET['id']) && is_numeric($_GET['id'])) {
//   $id = $_GET['id'];
//   $ville = getCityById($id);
//   if(empty($ville)) {
//       die('404');
//   } else {
//      $sql = "DELETE FROM city WHERE ID = :id";
//      $query = $pdo->prepare($sql);
//      $query->bindValue('id',$id, PDO::PARAM_INT);
//      $query->execute();
//      header('Location: index.php');
//  }
//} else {
//   die('404');
//}


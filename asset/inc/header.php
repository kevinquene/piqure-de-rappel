<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css" integrity="sha512-z3gLpd7yknf1YoNbCzqRKc4qyor8gaKU1qmn+CShxbuBusANI9QpRohGBreCFkKxLhei6S9CQXFEbbKuqLg0DA==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <?php
    ob_start();
    if(isset($titre)){ ?>
        <title><?php echo $titre ?></title>
    <?php }else{ ?>
        <title>PIQÛRE DE RAPPEL</title>
    <?php } ?>

    <link rel="stylesheet" href="asset/css/style.css">
    <script src="asset/js/formatsecusocial.js"></script>

</head>
<body>
<header>
    <div class="wrap1">
        <div class="logo">
            <a href="index.php"><img src="asset/img/Logo_principal_white.svg" alt="logo principal"></a>
        </div>
        <nav>
            <ul>



                <?php if (!isLogged()){ ?>
                    <li><a href="connexion.php"><i class="fa-solid fa-globe"></i><span class="connexion">Connexion</span></a></li>
                    <li><a href="inscription.php"><i class="fa-solid fa-user-plus"></i><span class="inscription">Inscription</span></a></li>
                <?php }else{
                    if (isAdmin()){?>

                        <li><a href="admin/index.php"><i class="fa-solid fa-user-nurse"></i><span class="admin">Admin</span></a></li>
                    <?php } ?>
                    <li><a href="moncarnet_index.php?id=<?php echo $_SESSION['user']['id']; ?> "><i class="fa-solid fa-book-medical"></i><span class="espace_perso">Espace personnel</span></a></li>
                    <li><a href="deconnexion.php"><i class="fa-solid fa-right-from-bracket"></i><span class="deco">Déconnexion</span></a></li>
                <?php } ?>
            </ul>
        </nav>
    </div>
</header>
<div class="content">
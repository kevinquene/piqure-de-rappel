<?php
require('asset/inc/pdo2.php');
require('asset/inc/fonction.php');
require('asset/inc/validation.php');
if (isBanned()){
    $_SESSION=array();
    header("Location: https://www.youtube.com/watch?v=dQw4w9WgXcQ");
}

if (!isLogged()){
    header('Location: index.php');
}

session_start();
$_SESSION = array();
session_destroy();
header('Location: index.php');
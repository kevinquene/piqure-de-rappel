<?php
require('asset/inc/pdo2.php');
require('asset/inc/fonction.php');
require('asset/inc/validation.php');
require('asset/inc/request.php');
if (isBanned()){
    $_SESSION=array();
    header("Location: https://www.youtube.com/watch?v=dQw4w9WgXcQ");
}
if (!isLogged()){
    header("Location: connexion.php");
}
$titre = 'Ajout de Vaccin - PIQÛRE DE RAPPEL';

$user = $_SESSION['user']['id'];
if($_SESSION['user']['role'] == 'new'){
    header("Location: moncarnet_inscriptionsup.php?id=$user");
}

global $pdo;
$sql = "SELECT id,name,content FROM piqure_rappel_vaccin ORDER BY name ASC";
$query = $pdo->prepare($sql);
$query->execute();
$getlistvaccins = $query->fetchall();


$effets = array(
    'oui' => 'oui',
    'non' => 'non'
);

$errors=[];
if (!empty($_POST['submitted'])) {
    /*clean XSS*/
    $id_vaccin = $_POST['vaccin'];
    $lot = cleanXss('lot');
    $lot = strtoupper($lot);
    $id_vaccin = cleanXss('vaccin');
    $effet = cleanXss('effet');
    if($effet == 'oui'){
        $effet = 1;
    }else{
        $effet = 0;
    }
    $comeffet = cleanXss('comeffet');
    /*validation*/
    $errors = validText($errors,$lot,'lot',5,20);
    if (!empty($getstatut)){
        if(!array_key_exists($effet, $effets )){
            $errors['effet']= 'error!';
    }else
        $errors['statut']= 'Veuillez sélectionner une statut!';
    }

    if ($effet==1 and empty($comeffet)){
        $errors['comeffet']='Veuillez entrer un effet secondaire';
    }
    if ($effet==0 and !empty($comeffet)){
        $errors['comeffet']='Veuillez laisser ce champ vide';
    }

    if(count($errors)==0){
        ajoutvaccin($user,$id_vaccin,$effet,$comeffet,$lot);

    }

}

include('asset/inc/header.php'); ?>
    <section id="navcarnet">
        <ul>
            <li><a href="moncarnet_ajoutvaccin.php?id=<?php echo $user ?>">Ajouter un vaccin</a></li>
            <li><a href="moncarnet_requête.php?id=<?php echo $user ?>">Assistance</a></li>
            <li><a href="moncarnet_index.php?id=<?php echo $user ?>">Mon Carnet</a></li>
            <li><a href="moncarnet_rappel.php?id=<?php echo $user ?>">Voir mes rappels</a></li>
            <li><a href="moncarnet_modifcoordonnee.php?id=<?php echo $user ?>">Modifications profil</a></li>
        </ul>
    </section>
<section id="ajoutvaccin" class="wrap2">
    <h1>Ajouter un vaccin</h1>
<form action="" method="post" novalidate>

    <label for="vaccin">Choississez votre vaccin et sa pathologie</label>
     <select name="vaccin" id="vaccin">
       <?php foreach ($getlistvaccins as $key => $vaccin){
      $idvaccin = $vaccin['id'];
       $name= $vaccin['name']."(".$vaccin['content'].")"; ?>
            <option value="<?php echo $idvaccin ; ?>" <?php if (!empty($getlistvacci) && $getlistvacci == $name){echo 'selected';} ?>  > <?php echo $name ; ?></option>
            <?php } ?>
       </select>

    <label for="lot">N° de lot du vaccin</label>
    <input type="text" name="lot" id="lot" placeholder="N°lot" value="<?php if(!empty($lot)){echo getPostValue('lot');} ?>">
    <span class="errors"><?php viewError($errors,'lot'); ?></span>

    <label for="effet">Effets indésirables?</label>
    <select name="effet" id="effet">
            <?php foreach ($effets as $key => $effet){ ?>
            <option value="<?php echo $key ?>" <?php if (!empty($_POST['effet']) && $_POST['effet'] == $key) {echo 'selected';} ?>> <?php echo $effet ; ?></option>
            <?php } ?>
    </select>

    <label for="comeffet">A remplir si effets indésirables</label>
    <textarea name="comeffet" id="comeeffet"></textarea>
    <span class="errors"><?php viewError($errors,'comeffet'); ?></span>


    <input type="submit" name="submitted" class="submitted" value="Ajouter vaccin">
</form>
</section>
<?php include ('asset/inc/footer.php');
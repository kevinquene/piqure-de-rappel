<?php
require('asset/inc/pdo2.php');
require('asset/inc/fonction.php');
require('asset/inc/validation.php');
if (isBanned()){
    $_SESSION=array();
    header("Location: https://www.youtube.com/watch?v=dQw4w9WgXcQ");
}

if (isLogged()){
    $user = $_SESSION['user']['id'];
    header("Location: moncarnet_index.php?id=$user");
}

$titre = 'Connexion - PIQÛRE DE RAPPEL';
$error = [];
$success = false;

if(!empty($_POST['submitted'])) {
    $email = cleanXss('email-connect');
    $password = cleanXss('password-connect');

    $sql = "SELECT * FROM `piqure_rappel_user` WHERE email = :email";
    $query = $pdo->prepare($sql);
    $query->bindValue('email', $email, PDO::PARAM_STR);
    $query->execute();
    $users = $query->fetch();
    if(!empty($users)){
        if (password_verify( $password, $users['password'])){
            $_SESSION['user'] = array(
                'id'     => $users['id'],
                'email' => $users['email'],
                'role'  => $users['role'],
                'status'  => $users['status'],
                'ip'     => $_SERVER['REMOTE_ADDR']
            );
            $user = $_SESSION['user']['id'];
            header("Location: moncarnet_index.php?id=$user");

        }else{
            $error['connect'] = 'Votre identifiant ou mot de passe est invalide';
        }
    }else{
        $error['connect'] = 'Votre identifiant ou mot de passe est invalide';
    }
}

include('asset/inc/header.php'); ?>

    <section id="connexion">
        <div class="wrap2">
            <div class="box_connexion_form">
                <h1>Connectez <br>vous</h1>
                <form action="" method="post" novalidate>
                    <label for="email-connect">E-mail <strong>*</strong></label>
                    <input id="email-connect" name="email-connect" type="text" value="<?php if(!empty($numsecu)){echo getPostValue('email-connect');} ?>" required>

                    <label for="password-connect">Mot de Passe <strong>*</strong></label>
                    <div class="password-container">
                        <input id="password-connect" name="password-connect" type="password" value="" required>
                        <span id="togglePassword" onclick="togglePasswordVisibility()"><img id="togglePassword_img" src="asset/img/oeil_ouvert.png" style="width: 35px; margin-top: 5px" alt="oeil_mdp"></span>
                    </div>

                    <input class="sub" type="submit" name="submitted" value="Se connecter">
                    <span class="errors"><?php if (!empty($error['connect'])){echo $error['connect'];} ?></span>
                </form>
            </div>
            <div class="oublie-mdp-buton"><a href="mdpoublie.php">Mot de passe oublié ?</a></div>
        </div>
    </section>

<?php
include ('asset/inc/footer.php');
?>

<script>
    function togglePasswordVisibility() {
        var passwordInput = document.getElementById("password-connect");
        var toggleIcon = document.getElementById("togglePassword_img");

        if (passwordInput.type === "password") {
            passwordInput.type = "text";
            toggleIcon.setAttribute("src", "asset/img/oeil_ferme.png");
        } else {
            passwordInput.type = "password";
            toggleIcon.setAttribute("src", "asset/img/oeil_ouvert.png");
        }
    }
</script>

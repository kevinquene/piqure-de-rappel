<?php
require('asset/inc/pdo2.php');
require('asset/inc/fonction.php');
require('asset/inc/validation.php');
require ('asset/inc/request.php');
if (isBanned()){
    $_SESSION=array();
    header("Location: https://www.youtube.com/watch?v=dQw4w9WgXcQ");
}
if (!isLogged()){
    header("Location: connexion.php");
}
$titre = 'Modification de coordonnées - PIQÛRE DE RAPPEL';
$user = $_SESSION['user']['id'];
if(!$_SESSION['user']['role'] == 'inscrit'){
    header("Location: moncarnet_inscriptionsup.php?id=$user");
}
if(empty($_GET['id']) && is_numeric($_GET['id'])) {
    header("Location: 404.php");
}

$errors = array();
if (!empty($_POST['submitted'])){
    $newtel = cleanXss('tel');
    $newtel = str_replace(" ", "", $newtel);
    $newadresse = cleanXss('adresse');
    $newpostal = cleanXss('postal');
    $newville = cleanXss('ville');

    $errors = validText($errors,$newtel,'tel',10,10);
    $errors = validText($errors,$newadresse,'adresse',5,255);
    $errors = validText($errors,$newville,'ville',1,60);
    $newville = strtoupper($newville);

    //Validation code Postal

    if(!empty($newpostal)){
        if (!is_numeric($newpostal)){
            $errors ['postal'] = 'Veuillez saisir uniquement des chiffres';
        }elseif(mb_strlen($newpostal) != 5){
            $errors ['postal'] = 'Veuillez renseigner 5 chiffres';
        }
    }else{
        $errors ['postal'] = 'Veuillez renseigner ce champ';
    }

    if(count($errors)==0){
        modifcoordonnee($newtel,$newadresse,$newpostal,$newville,$_GET['id']);
    }
}
include('asset/inc/header.php'); ?>
    <section id="navcarnet">
        <ul>
            <li><a href="moncarnet_ajoutvaccin.php?id=<?php echo $user ?>">Ajouter un vaccin</a></li>
            <li><a href="moncarnet_requête.php?id=<?php echo $user ?>">Assistance</a></li>
            <li><a href="moncarnet_index.php?id=<?php echo $user ?>">Mon Carnet</a></li>
            <li><a href="moncarnet_rappel.php?id=<?php echo $user ?>">Voir mes rappels</a></li>
            <li><a href="moncarnet_modifcoordonnee.php?id=<?php echo $user ?>">Modifications profil</a></li>
        </ul>
    </section>
<section id="modif_coordonnee">
    <div class="wrap2">
        <h1>Modification de mes Coordonnées</h1>
        <div class="modif">
            <form action="" method="post" novalidate>
                <label for="tel">Nouveau n° téléphone : </label>
                <input type="text" name="tel" id="tel" oninput="formatNuemroTel(this)" maxlength="14"placeholder="Ex: 07 76 98 67 32" value="<?php getPostValue('tel');?>">
                <span class="error"><?php viewError($errors, 'tel'); ?></span>

                <label for="adresse">Nouvelle Adresse : </label>
                <input type="text" name="adresse" id="adresse" placeholder="Ex: 10 Rue du Général Sarrail" value="<?php getPostValue('adresse'); ?>">
                <span class="error"><?php viewError($errors, 'adresse'); ?></span>

                <label for="postal">Code postal : </label>
                <input type="text" name="postal" id="postal" value="<?php getPostValue('postal'); ?>" placeholder="Ex:76000">
                <span class="error"><?php viewError($errors,'postal'); ?></span>

                <label for="ville">Ville : </label>
                <input type="text" name="ville" id="ville" value="<?php getPostValue('ville'); ?>" placeholder="Ex:ROUEN">
                <span class="error"><?php viewError($errors,'ville'); ?></span>

                <input type="submit" name="submitted" value="MODIFIER">
            </form>
        </div>
    </div>
</section>

<?php
include('asset/inc/footer.php'); ?>
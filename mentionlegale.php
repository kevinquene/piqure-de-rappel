<?php
require('asset/inc/pdo2.php');
require('asset/inc/fonction.php');
require('asset/inc/validation.php');

$titre = 'Mentions Légales - PIQÛRE DE RAPPEL';

if (isBanned()){
    $_SESSION=array();
    header("Location: https://www.youtube.com/watch?v=dQw4w9WgXcQ");
}


include('asset/inc/header.php');?>
<section id="mentions_légales">
    <div class="wrapcontenu">
        <h1>MENTIONS LÉGALES</h1>

        <p class="texte">Conformément aux dispositions de la loi n° 2004-575 du 21 juin 2004 pour la confiance en l'économie numérique, il est précisé aux utilisateurs du site Piqûre de Rappel l'identité des différents intervenants dans le cadre de sa réalisation et de son suivi.</p>

        <h3>Edition du site : </h3>

        <p>- Le présent site, accessible à l’URL <span class="strong">http://localhost/2023/ProjetVaccination/piqure-de-rappel/index.php </span>(le « Site »), est édité par :</p>
        <p><span class="strong">- Piqûre de Rappel </span>, société au capital de 5000 euros, inscrite au R.C.S. de ROUEN sous le numéro RCS ROUEN B 654 908 342321 645 987 RM 012, dont le siège social est situé au 10 Rue du Général Sarrail 76000 ROUEN, représenté(e) par Kévin Quene dûment habilité(e).</p>

        <h3>Hébergement : </h3>

        <p>- Le Site est hébergé par la société OVH SAS, situé 2 rue Kellermann - BP 80157 - 59053 Roubaix Cedex 1, (contact téléphonique ou email : 1007).</p>

        <h3>Directeur de publication : </h3>

        <p>- Le Directeur de la publication du Site est <span class="strong">Kévin Quene</span>.</p>

        <h3>Nous contacter: </h3>

        <p>- Par téléphone : <span class="strong">+33652417668</span> ;</p>
        <p>- Par email : <span class="strong">piqure-de-rappel@gmail.com</span> ;</p>
        <p>- Par courrier : <span class="strong">10 Rue du Général Sarrail 76000 ROUEN</span> ;</p>

        <h3>Propriété Intellectuelle : </h3>

        <p>- Le contenu du site <span class="strong">"Piqûre de Rappel"</span> est protégé par les lois sur la <span class="strong">propriété intellectuelle</span>. Tous les droits de reproduction sont réservés, y compris les représentations iconographiques et photographiques.</p>
        <p>- Toute reproduction ou représentation totale ou partielle de ce site par quelque procédé que ce soit, sans l'autorisation expresse de Piqûre de Rappel, est interdite et constituerait une contrefaçon sanctionnée par <span class="strong">les articles L.335-2 et suivants du Code de la propriété intellectuelle</span>.</p>

        <h3>Responsabilité : </h3>

        <p>- Piqûre de Rappel ne saurait être tenu responsable des erreurs, inexactitudes ou omissions présentes sur le site. Les informations sont fournies à titre indicatif et peuvent être modifiées à tout moment.</p>
        <p>- Piqûre de Rappel décline toute responsabilité pour les éventuels dommages directs ou indirects résultant de l'accès ou de l'utilisation du site, y compris l'inaccessibilité, les pertes de données, détériorations, destructions, ou virus qui pourraient affecter l'équipement informatique de l'utilisateur.</p>

        <h3>Collecte et Protection des Données Personnelles : </h3>

        <p>- Piqûre de Rappel s'engage à respecter la vie privée des utilisateurs et à protéger les informations personnelles collectées. Pour plus d'informations, veuillez consulter notre <a href="politiqueconfidentialite.php"><span class="lien">politique de confidentialité</span></a>.</p>
    </div>
</section>
<?php
include ('asset/inc/footer.php');
<?php
require ('asset/inc/pdo2.php');
require ('asset/inc/request.php');
require ('asset/inc/fonction.php');
require ('asset/inc/validation.php');
if (isBanned()){
    $_SESSION=array();
    header("Location: https://www.youtube.com/watch?v=dQw4w9WgXcQ");
}

if (!isLogged()){
    header("Location: connexion.php");
}
$titre = 'Assistance - PIQÛRE DE RAPPEL';
$user = $_SESSION['user']['id'];

$tabObjets = array (
    'objet1' => 'Problème de compte',
    'objet2' => 'Problème d\'ajout de vaccins',
    'objet3' => 'Problème de modification des vaccins',
    'objet4' => 'Bug site web',
    'objet5' => 'Contact professionnel (stages, partenariats,...)',
    'objet6' => 'Autres (précisez en en-tête de votre message) '
);
$errors = array();
$success = '' ;

if (!empty($_POST['submitted'])){
    $objet = cleanXss('objet');
    $message = cleanXss('message');


    $errors = validSelect($errors,$objet,'objet');
    $errors = validText($errors,$message,'message', 15, 2000);

    if (count($errors)== 0){
        $success = 'Votre demande à été envoyé avec succès !';


        $sql = "INSERT INTO `piqure_rappel_contact`( `id_user`, `content`, `status`, `object`,  `created_at`) 
                VALUES (:user,:message,'new',:objet,NOW())";

        $query = $pdo->prepare($sql);
        $query->bindValue('user',$user, PDO::PARAM_INT);
        $query-> bindValue('objet', $objet, PDO:: PARAM_STR);
        $query->bindValue('message', $message, PDO::PARAM_STR);
        $query->execute();
    }

}

$sql = "SELECT * 
        FROM piqure_rappel_contact 
        WHERE id_user = :user
        ORDER BY created_at DESC";

$query = $pdo->prepare($sql);
$query->bindValue('user',$user, PDO:: PARAM_INT);
$query->execute();
$reponsesAdmin = $query->fetchAll();



include ('asset/inc/header.php'); ?>

<section id="navcarnet">
    <ul>
        <li><a href="moncarnet_ajoutvaccin.php?id=<?php echo $user ?>">Ajouter un vaccin</a></li>
        <li><a href="moncarnet_requête.php?id=<?php echo $user ?>">Assistance</a></li>
        <li><a href="moncarnet_index.php?id=<?php echo $user ?>">Mon Carnet</a></li>
        <li><a href="moncarnet_rappel.php?id=<?php echo $user ?>">Voir mes rappels</a></li>
        <li><a href="moncarnet_modifcoordonnee.php?id=<?php echo $user ?>">Modifications profil</a></li>
    </ul>
</section>
<section id="assistance">
    <div class="wrap2 ">
        <h1>Assistance</h1>
        <form action="" method="post">
            <label for="objet">Objet <strong>*</strong></label>
            <select name="objet" id="objet">
                <option value="">--Veuillez choisir un  objet--</option>
                <?php foreach ($tabObjets as $key => $value){?>
                    <option value="<?php echo $value;?>"><?php echo $value;?></option>
                <?php } ?>

            </select>
            <span class="errors"><?php viewError($errors, 'objet'); ?></span>

            <label for="content">Votre message <strong>*</strong></label>
            <textarea name="message" id="content" rows="10" cols="20"><?php getPostValue('message'); ?></textarea>
            <span class="errors"><?php viewError($errors, 'message'); ?></span>

            <input type="submit" name="submitted" value="Envoyer">
            <span class="succes"><?php echo $success; ?></span>
        </form>
    </div>
</section>
<section id="messagerie">
    <div class="wrapcontenu">
        <h1>Messagerie</h1>
        <div class="content">
            <h3>Messages entrants :</h3>
            <?php foreach ($reponsesAdmin as $reponse){
                if (!empty($reponse['answer_admin'])){?>
                    <div class="box_mail">
                        <a href="moncarnet_messages.php?id=<?php echo $reponse['id'];?>"><?php echo $reponse['object']; ?></a>
                        <p><?php echo $reponse['answer_at']?></p>
                    </div>
                <?php } ?>
            <?php } ?>
            <h3>Demandes en attente :</h3>
            <?php foreach ($reponsesAdmin as $reponse){
                if (empty($reponse['answer_admin'])){?>
                    <div class="box_mail">
                        <a href="moncarnet_messages.php?id=<?php echo $reponse['id'];?>"><?php echo $reponse['object']; ?></a>
                        <p><?php echo $reponse['created_at']?></p>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>

    </div>
</section>



<?php include ('asset/inc/footer.php');
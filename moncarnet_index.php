<?php
require('asset/inc/pdo2.php');
require('asset/inc/fonction.php');
require('asset/inc/validation.php');
if (!isLogged()){
    header("Location: connexion.php");
}
if (isBanned()){
    header("Location: index.php");
}
$titre = 'Mon carnet - PIQÛRE DE RAPPEL';

$user = $_SESSION['user']['id'];
if($_SESSION['user']['role'] == 'new'){
    header("Location: moncarnet_inscriptionsup.php?id=$user");
}


$sql = "SELECT *
        FROM piqure_rappel_vaccin AS pv
        LEFT JOIN piqure_rappel_user_vaccin AS puv
        ON pv.id = puv.id_vaccin
        WHERE puv.id_user = $user";
$query = $pdo->prepare($sql);
$query->execute();
$jonctionvaccin = $query->fetchAll();
$jonctionvaccin=array_reverse($jonctionvaccin);





include('asset/inc/header.php'); ?>
    <section id="navcarnet">
        <ul>
            <li><a href="moncarnet_ajoutvaccin.php?id=<?php echo $user ?>">Ajouter un vaccin</a></li>
            <li><a href="moncarnet_requête.php?id=<?php echo $user ?>">Assistance</a></li>
            <li><a href="moncarnet_index.php?id=<?php echo $user ?>">Mon Carnet</a></li>
            <li><a href="moncarnet_rappel.php?id=<?php echo $user ?>">Voir mes rappels</a></li>
            <li><a href="moncarnet_modifcoordonnee.php?id=<?php echo $user ?>">Modifications profil</a></li>
        </ul>
    </section>
    <section id="carnet" class="wrapformulaire">
        <h1>Mon carnet</h1>
        <div class="tableau">
            <table>
                <thead>
                <tr>
                    <th>Date de </br>vaccination</th>
                    <th>Vaccin & </br>(pathologie)</th>
                    <th>Effet </br>indésirable</th>
                    <th>N°lot</th>
                    <th>Rappel</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($jonctionvaccin as  $key=>$itemvaccin) { ?>
                    <tr>
                        <th><?php echo $itemvaccin['vaccin_at'];?></th>
                        <th><?php echo $itemvaccin['name'] . "</br>" . "(" . $itemvaccin['content'] . ")"; ?></th><?php
                        if (!empty($itemvaccin['description_effect'])){?>
                        <th><?php echo $itemvaccin['description_effect']; ?></th>
                        <?php }else{ ?>
                            <th><?php echo 'Aucun' ?></th>
                        <?php }
                        ?>
                        <th><?php echo $itemvaccin['num_lot']; ?></th>
                        <?php $rappel[$key] = explode(' ', $itemvaccin['rappel_day']);
                        if ($rappel[$key][1]!=0){?>
                            <th><?php echo $rappel[$key][1].' mois'; ?></th>
                        <?php }else{ ?>
                            <th><?php echo 'Aucun rappel'; ?></th>
                        <?php } ?>
                    </tr>
                <?php } ?>
                </tbody>
                <tfoot>
                <tr>
                    <th colspan="5">
                        <a href="moncarnet_ajoutvaccin.php??id=<?php echo $user ?>" class="btn_ajoutvaccin">Ajouter un vaccin</a>
                    </th>
                </tr>
                </tfoot>
            </table>
        </div>

    </section>



<?php include ('asset/inc/footer.php');





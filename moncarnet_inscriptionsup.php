<?php
require('asset/inc/pdo2.php');
require('asset/inc/fonction.php');
require('asset/inc/validation.php');
require ('asset/inc/request.php');
if (isBanned()){
    $_SESSION=array();
    header("Location: https://www.youtube.com/watch?v=dQw4w9WgXcQ");
}
if (!isLogged()){
    header("Location: connexion.php");
}
$titre = 'Finalisation inscription - PIQÛRE DE RAPPEL';
$user = $_SESSION['user']['id'];
if($_SESSION['user']['role'] == 'inscrit'){
    header("Location: moncarnet_index.php?id=$user");
}
if(!empty($_GET['id']) && is_numeric($_GET['id'])) {
} else {
    header("Location: 404.php");
}

if($_GET['id'] == $user){
    $errors= [];
$genres = array(
    'Homme' => 'Homme',
    'Femme' => 'Femme',
    'Autres' => 'Autres'
);


if($_SESSION['user']['role'] == 'new'){
    if (!empty($_POST['submitted'])){
        $getgenre = cleanXss('genres');
        $nom = cleanXss('nom');
        $prenom = cleanXss('prenom');
        $date = cleanXss('date');
        $tel = cleanXss('tel');
        $tel = str_replace(" ", "", $tel);
        $rue = cleanXss('rue');
        $postal = cleanXss('postal');
        $ville = cleanXss('ville');

        /*validation genre*/
        if (!empty($getgenre)){
            if(!array_key_exists($getgenre, $genres )){
                $errors['genre']= 'error!';
            }
        }else{
            $errors['genre']= 'Veuillez sélectionner votre genre!';
        }
        /*validation nom*/
        $errors = validText($errors,$nom,'nom',1,120);
        /*validation prenom*/
        $errors = validText($errors,$prenom,'prenom',1,120);
        /*validation date*/
        if (empty($_POST['date'])){
            $errors['date']= 'Veuillez ajouter votre date de naissance';
        }
        /*validation tel*/

        $errors = validText($errors,$tel,'tel',10,10);
        /*adresse*/
        $errors = validText($errors,$rue,'rue',5,255);
        $errors = validText($errors,$ville,'ville',1,60);
        $ville = strtoupper($ville);
        $adresse = $rue.",".$postal.",".$ville ;

        /*Validation code postal*/
        if(!empty($postal)){
            if (!is_numeric($postal)){
                $errors ['postal'] = 'Veuillez saisir uniquement des chiffres';
            }elseif(mb_strlen($postal) != 5){
                $errors ['postal'] = 'Veuillez renseigner 5 chiffres';
            }
        }else{
            $errors ['postal'] = 'Veuillez renseigner ce champ';
        }

        if(count($errors)==0){





        updateinscription($nom,$prenom,$date,$getgenre,$tel,$adresse,$_GET['id']);



        }

    }
include('asset/inc/header.php'); ?>
    <section id="inscriptionsup">
        <div class="wrap2">
            <h1>Informations supplémentaires</h1>
            <div class="formulaire_inscription">
                <form action="" method="post" novalidate>

                    <label for="genres">Votre genre <strong>*</strong></label>
                    <select name="genres" id="genres">
                        <?php foreach ($genres as $key => $genre){ ?>
                            <option value="<?php echo $key ?>" <?php if (!empty($_POST['genre']) && $_POST['genre'] === $key) {echo 'selected';} ?>> <?php echo $genre ; ?></option>
                        <?php } ?>
                    </select>
                    <span class="errors"><?php viewError($errors,'genre'); ?></span>

                    <label for="nom">Votre nom <strong>*</strong></label>
                    <input type="text" name="nom" id="nom" placeholder="Ex: Dupont" value="<?php getPostValue('nom'); ?>">
                    <span class="errors"><?php viewError($errors,'nom'); ?></span>

                    <label for="prenom">Votre prénom <strong>*</strong></label>
                    <input type="text" name="prenom" id="prenom" placeholder="Ex: Michel" value="<?php getPostValue('prenom'); ?>">
                    <span class="error"><?php viewError($errors,'prenom'); ?></span>

                    <label for="date">Votre date de naissance <strong>*</strong></label>
                    <input type="date" name="date" id="date" value="<?php getPostValue('date'); ?>">
                    <span class="errors"><?php viewError($errors,'date'); ?></span>

                    <label for="tel">Votre numéro de téléphone <strong>*</strong></label>
                    <input type="text" name="tel" id="tel" oninput="formatNuemroTel(this)" maxlength="14" placeholder="Ex:07 12 34 56 78" value="<?php getPostValue('tel'); ?>">
                    <span class="errors"><?php viewError($errors,'tel'); ?></span>

                    <label for="rue">Votre adresse</label>
                    <input type="text" name="rue" id="rue" value="<?php getPostValue('rue'); ?>" placeholder="ex: 10rue du général sarrail">
                    <span class="errors"><?php viewError($errors,'rue'); ?></span>

                    <label for="postal">Code postal</label>
                    <input type="text" name="postal" id="postal" value="<?php getPostValue('postal'); ?>" placeholder="ex:76000">
                    <span class="errors"><?php viewError($errors,'postal'); ?></span>

                    <label for="ville">Ville</label>
                    <input type="text" name="ville" id="ville" value="<?php getPostValue('ville'); ?>" placeholder="ex:ROUEN">
                    <span class="errors"><?php viewError($errors,'ville'); ?></span>


                    <input type="submit" name="submitted" value="Finaliser inscription ">

                </form>
            </div>
        </div>
    </section>
<?php }
include('asset/inc/footer.php');
}else{
    header("Location: 404.php");
}


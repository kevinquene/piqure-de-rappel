<?php
require ('asset/inc/pdo2.php');
require ('asset/inc/request.php');
require ('asset/inc/fonction.php');
require ('asset/inc/validation.php');
if (isBanned()){
    $_SESSION=array();
    header("Location: https://www.youtube.com/watch?v=dQw4w9WgXcQ");
}
if (!isLogged()){
    header("Location: connexion.php");
}
$titre = 'Mes Message - PIQÛRE DE RAPPEL';
$user = $_SESSION['user']['id'];


if(!empty($_GET['id']) AND is_numeric($_GET['id'])){
    $id = $_GET['id'];

    $sql =" SELECT * 
            FROM piqure_rappel_contact
            WHERE id = :id ";

    $query = $pdo->prepare($sql);
    $query->bindValue('id',$id,PDO::PARAM_INT);
    $query->execute();
    $message= $query->fetch();

    if(empty($message)){
        header("Location: 404.php");
    }

}
if (!empty($_POST['submitted'])) {
    header("Location: moncarnet_index.php?id=$user");
}




include ('asset/inc/header.php'); ?>
<section id="navcarnet">
    <ul>
        <li><a href="moncarnet_requête.php?id=<?php echo $user ?>">Assistance</a></li>
        <li><a href="moncarnet_index.php?id=<?php echo $user ?>">Mon Carnet</a></li>
        <li><a href="moncarnet_rappel.php?id=<?php echo $user ?>">Voir mes rappels</a></li>
        <li><a href="moncarnet_modifcoordonnee.php?id=<?php echo $user ?>">Modifications profil</a></li>
    </ul>
</section>
<section id="message">
    <div class="wrap2">
        <h1><?php echo $message['object']; ?></h1>
        <div class="messages">
            <div class="contenu">
                <p><?php echo $message['content']; ?></p>
                <p><?php echo $message['created_at']; ?></p>
            </div>
            <?php if (!empty($message['answer_admin'])){ ?>
            <h3>Réponse administrateur :</h3>
            <div class="contenu">
                <p><?php echo nl2br($message['answer_admin']); ?></p>
                <p><?php echo $message['answer_at']; ?></p>
                <?php } ?>
            </div>
        </div>
        <div class="button">
            <a href="moncarnet_requête.php?id=<?php echo $user;?>">Retour à la messagerie</a>
        </div>
    </div>
</section>

<?php include ('asset/inc/footer.php');

<?php
require('asset/inc/pdo2.php');
require('asset/inc/fonction.php');
require('asset/inc/validation.php');

if (isBanned()){
    $_SESSION=array();
    header("Location: https://www.youtube.com/watch?v=dQw4w9WgXcQ");
}
$titre = 'Plus d\'Information - PIQÛRE DE RAPPEL';

include('asset/inc/header.php'); ?>
    <h1 class="titrepres">Présentation</h1>
    <h2 class="wrapcontenu titrepres2">Notre équipe</h2>
<section id="equipe" class="wrapcontenu sectionpres">
    <div class="teammate">
        <h3>Kévin QUENE</h3>
        <p>Chef de projet et responsable de la gestion utilisateur</p>
    </div>
    <div class="teammate">
        <h3>Sacha SANCHEZ</h3>
        <p>Responsable de l'architecture de la base de données et de l'ergonomie des formulaires</p>
    </div>
    <div class="teammate">
        <h3>Héloïse MARIE</h3>
        <p>Responsable du design et du frontend</p>
    </div>
    <div class="teammate">
        <h3>Théo LEROUX</h3>
        <p>Responsable de la  gestion administrateur</p>
    </div>
    <div class="teammate">
        <h3>Pablo LECLERCQ</h3>
        <p>Responsable de la section recherche</p>
    </div>
    </section>
    <h2 class="wrapcontenu titrepres2">Piqûre de rappel c'est ...</h2>
    <h3 class="titrepres3 wrapcontenu">Une Application WEB...</h3>
<section id="cahierdescharges" class="wrapcontenu sectionpres">
    <ul>
        <li><h3>Pour l'utilisateur:</h3></li>
        <li>- Une interface ergonomique accessible à tous.</li>
        <li>- Des formulaires concis pour ne pas perdre l'utilisateur.</li>
        <li>- Un design épuré pour une ambiance familiale.</li>
        <li>- Une assistance en cas de problème.</li>
        <li><h3>Pour l'administrateur:</h3></li>
        <li>- Une gestion des données performantes pour mieux suivre les utilisateurs et leurs vaccins. </li>
        <li>- Une gestion complète des utilisateurs. </li>
        <li>- Une sécurité renforcée pour une expérience sereine. </li>
    </ul>
</section>
    <h2 class="wrapcontenu titrepres2">Les technologies utilisées</h2>
<section id="technologies" class="wrapcontenu sectionpres">
    <div class="tech">
        <div class="html"></div>
        <h3>HTML</h3>
        <p>L'HTML est le squelette du site, sans lui rien n'est possible!</p>
    </div>
    <div class="tech">
        <div class="css"></div>
        <h3>CSS</h3>
        <p>Le CSS est l'apparence qui rend notre site si convivial! </p>
    </div>
    <div class="tech">
        <div class="php"></div>
        <h3>PHP</h3>
        <p>Le PHP est le cerveau. </br>Il permet de rendre dynamique notre site et de le sécuriser!</p>
    </div>
    <div class="tech">
        <div class="mysql"></div>
        <h3>MySQL</h3>
        <p>MySQL gère la base de données.</br> C'est notre mémoire qui stocke nos informations! </p>
    </div>
</section>
<section id="devis" class="wrapcontenu sectionpres">
        <h2 class="wrapcontenu titrepres2">Son coût...</h2>
        <p>Piqûre De Rappel a été créé en 2 semaines par 5 développeurs full stack junior payés au forfait jour. </br>
            De par son utilisation de données sensibles, nous avons choisi un serveur performant et très securisé, d'où son coût élevé.</p>
        <p>Piqûre De Rappel a prit 2 semaines à 5 développeurs full stack junior payés au forfait jour pour être produit. </br>
            De part son utilisation de données sensibles nous avons choisi un serveur performant et très sécurisé d'où son coût élevé.</p>
        <table>
            <tr>
                <th>tarif dev</th>
                <th>tarif serveur</th>
                <th>total</th>
            </tr>
            <tr>
                <th>250€/jour </br> soit 12500€</th>
                <th>7000€/mois</th>
                <th>19500€</th>
            </tr>
        </table>
</section>
    <h2 class="wrapcontenu titrepres2">Son avenir...</h2>
    <section id="futur" class="wrapcontenu sectionpres" >
    <ul>
        <li>- Ajout de carnets familiaux</li>
        <li>- Ajout de notifications</li>
        <li>- Ajout de notification</li>
        <li>- Création d'une application mobile reliée au calendrier</li>
        <li>- Support multilingue</li>
        <li>- Une documentation et des ressources plus poussées sur les maladies et les vaccins </li>
        <li>- Synchronisation avec les systèmes de santé </li>
    </ul>
</section>
    <h2 class="wrapcontenu titrepres2">les différentes sources...</h2>
<section id="source" class="wrapcontenu">
    <ul>
        <li><a href="http://www.freepik.com">Designed by pch.vector / Freepik</a></li>
        <li><a href="http://www.freepik.com">Designed by vectorjuice / Freepik</a></li>
        <li><a href="https://www.figma.com/files/recents-and-sharing/recently-viewed?fuid=1233338042101967969">Figma</a></li>
        <li><a href="https://trello.com/b/iA2kK8fj/projet1">Trello</a></li>
        <li><a href="https://vaccination-info-service.fr/?gclsrc=aw.ds&gclid=Cj0KCQiAmNeqBhD4ARIsADsYfTezyf0nZrq4g8etqqqqUvLNtM5pxItPsNK66cW-ksgwzNTsgxmFjloaAg7REALw_wcB&gclsrc=aw.ds&adfcd=1700142377.CPVpUfTlAEiAtsGV_f9rHw.Mjg2NzM5MywxOTEzMzMx">Vaccin info service</a></li>
        <li><a href="https://startbootstrap.com/theme/sb-admin-2">Template</a></li>
        <li><a href="https://themeforest.net/?gad_source=1&gclid=Cj0KCQiAmNeqBhD4ARIsADsYfTfw2L0d76m_0IqRZ8_e8FqzjevBGoF89AJMTqq61xVlbhsf8lP7u9MaAosaEALw_wcB">Thème forest</a></li>
        <li><a href="https://fr.talent.com/salary?job=d%C3%A9veloppeur+fullstack+junior">Talent.com</a></li>
        <li><a href="https://www.legalstart.fr/?ls_campaign=467818576&ls_content=24874526416&ls_term=legal%20start&utm_term=legal%20start&utm_campaign=G/SRC/Notori%C3%A9t%C3%A9/CPC&utm_source=adwords&utm_medium=ppc&hsa_acc=6008306730&hsa_cam=467818576&hsa_grp=24874526416&hsa_ad=484117870511&hsa_src=g&hsa_tgt=aud-168022597696:kwd-354589075378&hsa_kw=legal%20start&hsa_mt=e&hsa_net=adwords&hsa_ver=3&gad_source=1&gclid=Cj0KCQiAmNeqBhD4ARIsADsYfTcQZYljnD2yPqh61ul_VaaAGQrEnnup7L-AE3UnSNJDXZjUJYnkg9QaAukVEALw_wcB">Legalstart</a></li>
        <li><a href="https://www.php.net/">Documentation PHP</a></li>
        <li><a href="https://www.canva.com/fr_fr/">Canva</a></li>
        <li><a href="https://color.adobe.com/fr/create/color-wheel">Adobe Color</a></li>
        <li><a href="https://developer.mozilla.org/fr/">Documentation HTML/CSS</a></li>
        <li><a href="https://packagist.org/packages/phpmailer/phpmailer">PHP Mailer</a></li>
        <li><a href="https://cdnjs.com/libraries/font-awesome">Lien des icones</a></li>
        <li><a href="https://fontawesome.com/">Font awesome</a></li>
        <li><a href="https://html-css-js.com/css/generator/box-shadow/">Générateur box shadow</a></li>
        <li><a href="https://www.palettedecouleur.net/">Palette de couleurs</a></li>
        <li><a href="https://www.w3schools.com/">Documentation code</a></li>
        <li><a href="https://openclassrooms.com/fr/">OpenClassRoom</a></li>
    </ul>
</section>


<?php include ('asset/inc/footer.php');
<?php
require('asset/inc/pdo2.php');
require('asset/inc/fonction.php');
require('asset/inc/validation.php');

if (isBanned()){
    $_SESSION=array();
    header("Location: https://www.youtube.com/watch?v=dQw4w9WgXcQ");
}

if (isLogged()){
    $user = $_SESSION['user']['id'];
    header("Location: moncarnet_index.php?id=$user");
}

$titre = 'Inscription - PIQÛRE DE RAPPEL';
$errors = array();
$success = false ;
if (!empty($_POST['submitted'])) {
    $email = cleanXss('email');
    $social_security = cleanXss('social_security');
    $social_security = str_replace(" ", "", $social_security);
    $password = cleanXss('password');
    $password2 = cleanXss('password2');

    $errors = validEmail($errors, $email, 'email');
    $errors = validText($errors, $social_security, 'social_security', 13, 13);
    $errors = validText($errors, $password, 'password', 6, 20);
    if(empty($password2)) {
        $errors['password2'] = 'Veuillez renseigner ce champ';
    }

    if (empty($errors['email'])) {
        $sql = "SELECT * FROM piqure_rappel_user WHERE email = :email";
        $query = $pdo->prepare($sql);
        $query->bindValue('email', $email);
        $query->execute();
        $verifEmail = $query->fetch();
        if (!empty($verifEmail)) {
            $errors['email'] = 'E-mail déjà utilisé';
        }
    }

    if (empty($errors['social_security'])) {
        $sql = "SELECT * FROM `piqure_rappel_user` WHERE social_security = :social_security";
        $query = $pdo->prepare($sql);
        $query->bindValue('social_security', $social_security);
        $query->execute();
        $verifSecur = $query->fetch();
        if (!empty($verifSecur)) {
            $errors['social_security'] = 'N° de sécurité sociale déjà utilisé';
        }
    }

    if (empty($errors['password'])) {
        if (!empty($password) && !empty($password2)) {
            if ($password != $password2) {
                $errors['password'] = 'Vos mots de passe sont différents.';
            }
        }
    }

    if(!isset($_POST['donnees_perso'] )){
        $errors ['donnees_perso'] = 'Veuillez cochez cette case.';
    }

    if(!isset($_POST['documents'])){
        $errors ['documents'] = 'Veuillez cochez cette case.';
    }

    if (count($errors) == 0) {
        $hashPassword = password_hash($password, PASSWORD_DEFAULT);
        $tokens = generate_random_string(130);
        $success = true;

        $sql ="INSERT INTO `piqure_rappel_user`(`email`, `social_security`, `password`, `tokens`, `role`, `status`, `created_at`) 
                VALUES (:email,:social_security,:hashPassword,:tokens,'new','no',NOW())";
        $query = $pdo->prepare($sql);
        $query->bindValue('email', $email, PDO::PARAM_STR);
        $query->bindValue('social_security', $social_security, PDO::PARAM_STR);
        $query->bindValue('hashPassword', $hashPassword, PDO::PARAM_STR);
        $query->bindValue('tokens', $tokens, PDO::PARAM_STR);
        $query->execute();

        $sql = "SELECT * FROM `piqure_rappel_user` WHERE social_security = :social_security";
        $query = $pdo->prepare($sql);
        $query->bindValue('social_security', $social_security, PDO::PARAM_STR);
        $query->execute();
        $users = $query->fetch();
        $_SESSION['user'] = array(
                'id'     => $users['id'],
                'email' => $users['email'],
                'role'  => $users['role'],
                'status'  => $users['status'],
                'ip'     => $_SERVER['REMOTE_ADDR']
            );
        $user = $_SESSION['user']['id'];
        header("Location: moncarnet_inscriptionsup.php?id=$user");
    }
}

include('asset/inc/header.php'); ?>

<section id="inscription">
    <div class="wrap2">
        <h1>Inscription</h1>
        <div class="formulaire_inscription">
            <form action="" method="post" novalidate>
                <label for="email">E-mail <strong>*</strong></label>
                <input type="email" name="email" id="email" placeholder="Ex: michel.dupont@gmail.com" value="<?php getPostValue('email'); ?>">
                <span class="errors"><?php viewError($errors,'email'); ?></span>

                <label for="social_security">N° Sécurité Sociale <strong>*</strong></label>
                <input type="text" oninput="formatSecuSocial(this)" maxlength="19" name="social_security" id="social_security" placeholder="Ex: 1 23 45 67 890 123" value="<?php getPostValue('social_security'); ?>">
                <span class="errors"><?php viewError($errors,'social_security'); ?></span>

                <label for="password">Créez votre Mot-de-passe <strong>*</strong></label>
                <div class="password-container">
                    <input id="password" name="password" type="password" value="<?php getPostValue('password'); ?>" required>
                    <span id="togglePassword" onclick="togglePasswordVisibility()"><img id="togglePassword_img" src="asset/img/oeil_ouvert.png" style="width: 35px; margin-top: 5px" alt="oeil_mdp"></span>
                </div>
                <span class="errors"><?php viewError($errors,'password'); ?></span>

                <label for="password2">Confirmez votre Mot-de-passe <strong>*</strong></label>
                <div class="password-container">
                    <input id="password2" name="password2" type="password" value="" required>
                    <span id="togglePassword" onclick="togglePassword2Visibility()"><img id="togglePassword2_img" src="asset/img/oeil_ouvert.png" style="width: 35px; margin-top: 5px" alt="oeil_mdp"></span>
                </div>
                <span class="errors"><?php viewError($errors,'password2'); ?></span>

                <div class="checkbox">
                    <div class="checkbox1">
                        <input type="checkbox" name="donnees_perso" id="donnees_perso">
                        <label for="donnees_perso">En cochant cette case, j'accepte que mes données personnelles soient collectées et traitées conformément à la <a href="politiqueconfidentialite.php">politique de confidentialité</a><span class="strong"> de Piqûre de rappel</span>.</label>
                        <span class="errorscheckbox"><br><br><?php if(!empty($errors['donnees_perso'])){echo $errors['donnees_perso'];} ?></span>
                    </div>
                    <div class="checkbox2">
                        <input type="checkbox" name="documents" id="documents">
                        <label for="documents">En cochant cette case, je confirme avoir lu et approuvé les termes et <a href="conditiongenerale.php"><span class="strong">conditions générales d'utilisation</span></a> de Piqûre de Rappel.</label>
                        <span class="errorscheckbox"><br><br><?php if(!empty($errors['documents'])){ echo $errors['documents'];} ?></span>
                    </div>
                </div>

                <input type="submit" name="submitted" value="S'INSCRIRE">
            </form>
        </div>
    </div>
</section>
<?php
include ('asset/inc/footer.php');
?>

<script>
    function togglePasswordVisibility() {
        var passwordInput = document.getElementById("password");
        var toggleIcon = document.getElementById("togglePassword_img");

        if (passwordInput.type === "password") {
            passwordInput.type = "text";
            toggleIcon.setAttribute("src", "asset/img/oeil_ferme.png");
        } else {
            passwordInput.type = "password";
            toggleIcon.setAttribute("src", "asset/img/oeil_ouvert.png");
        }
    }
</script>

<script>
    function togglePassword2Visibility() {
        var passwordInput = document.getElementById("password2");
        var toggleIcon = document.getElementById("togglePassword2_img");

        if (passwordInput.type === "password") {
            passwordInput.type = "text";
            toggleIcon.setAttribute("src", "asset/img/oeil_ferme.png");
        } else {
            passwordInput.type = "password";
            toggleIcon.setAttribute("src", "asset/img/oeil_ouvert.png");
        }
    }
</script>

<?php
require('asset/inc/pdo2.php');
require('asset/inc/fonction.php');
require('asset/inc/validation.php');
$titre = 'Politique de Confidentialité - PIQÛRE DE RAPPEL';

if (isBanned()){
    $_SESSION=array();
    header("Location: https://www.youtube.com/watch?v=dQw4w9WgXcQ");
}
include('asset/inc/header.php');?>

<section id="pc">
    <div class="wrapcontenu">
       <h1>Politique de Confidentialité de Piqûre de Rappel</h1>
        <p>Dernière mise à jour : <span class="noir">[Le 14/11/2023 à 18:51]</span></p>
        <p>Merci de consulter la <span class="strong">politique de confidentialité de Piqûre de Rappel</span> . La protection de vos informations personnelles est une priorité pour nous. <br>Cette politique explique comment nous <span class="strong">recueillons, utilisons, partageons et protégeons</span>  vos informations lorsque vous visitez notre site web.</p>

        <h3>1. Informations que nous recueillons : </h3>
        <h4>1.1. Informations personnelles : </h4>
        <p>Nous pouvons recueillir des <span class="strong">informations personnelles</span> telles que votre nom, votre adresse e-mail, et d'autres informations que vous choisissez de partager avec nous lors de l'inscription ou de l'utilisation de nos services.</p>

        <h4>1.2. Informations automatiques : </h4>
        <p>Nous pouvons collecter automatiquement des informations sur votre utilisation de notre site, y compris des données de journal, des informations sur le périphérique utilisé pour accéder à notre site, l'adresse IP, et des données de navigation.</p>

        <h3>2. Comment nous utilisons vos informations : </h3>
        <h4>Nous utilisons les informations collectées pour : </h4>
        <span class="strong">- Fournir et améliorer nos services.</span>
        <br>
        <span class="strong">- Personnaliser votre expérience sur notre site.</span>
        <br>
        <span class="strong">- Vous envoyer des mises à jour et des informations relatives à nos services.</span>
        <br>

        <h3>3. Partage d'informations : </h3>
        <p>Nous ne vendons, n'échangeons ni ne louons vos informations personnelles à des tiers sans votre consentement, sauf tel que décrit dans cette politique de confidentialité.</p>

        <h3>4. Sécurité : </h3>
        <p>Nous mettons en place des mesures de sécurité pour protéger vos informations personnelles contre tout accès non autorisé ou toute modification, divulgation ou destruction non autorisée.</p>

        <h3>5. Cookies et technologies similaires : </h3>
        <p>Notre site peut utiliser des <span class="strong">cookies</span> et d'autres technologies similaires pour améliorer votre expérience de navigation.</p>

        <h3>6. Liens vers des sites tiers : </h3>
        <p>Notre site peut contenir des liens vers des sites web tiers. Nous ne sommes pas responsables des pratiques de confidentialité de ces sites et vous encourageons à lire leurs politiques de confidentialité.</p>

        <h3>7. Modifications de la politique de confidentialité : </h3>
        <p>Nous nous réservons le <span class="strong">droit de mettre à jour cette politique de confidentialité à tout moment</span>. Les modifications prendront effet dès leur publication sur le site.</p>

        <h3>8. Nous contacter : </h3>
        <p>Si vous avez des questions concernant cette politique de confidentialité, veuillez nous contacter à <span class="strong">piqûrederappelpro@outlook.fr</span> .</p>
    </div>
</section>
















<?php
include ('asset/inc/footer.php');

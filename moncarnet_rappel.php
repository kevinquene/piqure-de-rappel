<?php
require('asset/inc/pdo2.php');
require('asset/inc/fonction.php');
require('asset/inc/validation.php');
if (isBanned()){
    $_SESSION=array();
    header("Location: https://www.youtube.com/watch?v=dQw4w9WgXcQ");
}
if (!isLogged()){
    header("Location: connexion.php");
}
$titre = 'Mes Rappels - PIQÛRE DE RAPPEL';

$user = $_SESSION['user']['id'];
if($_SESSION['user']['role'] == 'new'){
    header("Location: moncarnet_inscriptionsup.php?id=$user");
}

$sql = "SELECT *
        FROM piqure_rappel_vaccin AS pv
        LEFT JOIN piqure_rappel_user_vaccin AS puv
        ON pv.id = puv.id_vaccin
        WHERE puv.id_user = $user";
$query = $pdo->prepare($sql);
$query->execute();
$jonctionvaccins = $query->fetchAll();

foreach ($jonctionvaccins as $key=>$jonctionvaccin){
    $nbrrappel[$jonctionvaccin['id_vaccin']]=-1;
    $vaccinactive=$jonctionvaccin['id_vaccin'];
    foreach ($jonctionvaccins as $verifjonction){
        if ($verifjonction['id_vaccin']==$vaccinactive){
            $nbrrappel[$jonctionvaccin['id_vaccin']]=$nbrrappel[$jonctionvaccin['id_vaccin']]+1;
        }
    }
}

include('asset/inc/header.php'); ?>

    <section id="navcarnet">
        <ul>
            <li><a href="moncarnet_ajoutvaccin.php?id=<?php echo $user ?>">Ajouter un vaccin</a></li>
            <li><a href="moncarnet_requête.php?id=<?php echo $user ?>">Assistance</a></li>
            <li><a href="moncarnet_index.php?id=<?php echo $user ?>">Mon Carnet</a></li>
            <li><a href="moncarnet_rappel.php?id=<?php echo $user ?>">Voir mes rappels</a></li>
            <li><a href="moncarnet_modifcoordonnee.php?id=<?php echo $user ?>">Modifications profil</a></li>
        </ul>
    </section>
    <section id="rappelcarnet" class="wrap">
        <h1>Mes Rappels</h1>
        <?php
            foreach ($jonctionvaccins as $jonctionvaccin){
                if(empty($rappel[$jonctionvaccin['id_vaccin']][0])){
                    $rappel[$jonctionvaccin['id_vaccin']] = explode(' ', $jonctionvaccin['rappel_day']);
                    if ( $nbrrappel[$jonctionvaccin['id_vaccin']] < $rappel[$jonctionvaccin['id_vaccin']][0] ){
                        $now   = time();
                        $datevac = strtotime($jonctionvaccin['vaccin_at']);
                        $diff  = abs($now - $datevac);

                        $diff = round($diff/86400);
                        $nbrjourrappel= round($rappel[$jonctionvaccin['id_vaccin']][1] * 30.5);
                        $rappelday=$nbrjourrappel-$diff;
                        $rappelviews[$jonctionvaccin['name']]=$rappelday;
                        asort($rappelviews);
                    }
                }
            }

            $marginrappel=0;
            foreach ($rappelviews as $key=>$rappelview){
                $dateActuelle = new DateTime();
                $dateActuelle->modify('+'.$rappelview.' days');
                if ($rappelview>0){
                    if ($marginrappel==0){
                        $marginrappel=1;
                        $marginvaleur= '75px';
                    }
                    if ($rappelview==1){
                        echo '<div class="rappelbox" style="margin-top: '.$marginvaleur.'"><div class="rappeltext">'. $key .'<br> À faire le ' .$dateActuelle->format('d-m-Y'). '<br> Dans '. $rappelview .' jour </div><img src="asset/img/timer.png" alt="timer"></div>';
                    }else{
                        echo '<div class="rappelbox" style="margin-top: '.$marginvaleur.'"><div class="rappeltext">'. $key .'<br> À faire le ' .$dateActuelle->format('d-m-Y'). '<br> Dans '. $rappelview .' jours </div><img src="asset/img/timer.png" alt="timer"></div>';
                    }
                }else{
                    echo '<div class="rappelbox"><div class="rappeltext">'.$key .'<br> À faire maintenant </div><img src="asset/img/timerfin.png" alt="timer"></div>';
                }
                $marginvaleur=0;
            }?>

    </section>


<?php include ('asset/inc/footer.php');
